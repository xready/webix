#!/bin/sh
echo "Check webix directory"
if [ ! -d "webix" ]
then
  mkdir webix
  fi

cd webix

echo "Load or reload Webix PRO Trial"
if [ -d "trial" ]
then
  echo "Remove current directory"
  rm -rf trial
  fi

echo "Download Webix"
wget --quiet --no-check-certificate https://webix.com/packages/webix.trial.complete.zip

if [ ! -f "webix.trial.complete.zip" ]
then
  echo "Download error";
  exit 0;
  fi

echo "Unpack Webix"
unzip -q webix.trial.complete.zip -d webix.trial
echo "Move to correct directory"
mv webix.trial/webix/codebase trial
echo "Remove redundant"
rm -rf webix.trial
rm -rf webix.trial.complete.zip