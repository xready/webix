XR.Webix.mixins.Events = {
    attachEvent(event, handler) {
        if (XR.isFunction(handler)) {
            if (!XR.exists(event, this.events)) {
                this.events[event] = [];
            }

            this.events[event].push(handler);
        }
    },

    fireEvent(event, ...args) {
        if (XR.exists(event, this.events)) {
            for (let i in this.events[event]) {
                if (this.events[event][i](this, ...args) === false) {
                    return false;
                }
            }
        }
    },

    applyEvents() {
        let config = this.getViewConfig();
        
        if (XR.notExists('on', config)) {
            config.on = {};
        }

        for (let event in this.events) {
            config.on[event] = (...args) => {
                return this.fireEvent(event, ...args)
            }
        }
    },

    afterBuild(callback) {
        XR.Webix.addWidgetBuildCallback(callback);
    }
}