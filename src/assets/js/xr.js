XR = {
    isTypeOf(value, type) {
        return (typeof value === type);
    },

    isSet(value) {
        return !this.isTypeOf(value, 'undefined');
    },

    isObject(value, arrayFalse) {
        arrayFalse = this.setDefault(arrayFalse, true);

        if (arrayFalse) {
            return this.isTypeOf(value, 'object') && !this.isArray(value);
        }

        return this.isTypeOf(value, 'object');
    },

    isArray(value) {
        return Array.isArray(value);
    },

    isFunction(value) {
        return this.isTypeOf(value, 'function');
    },

    isString(value) {
        return this.isTypeOf(value, 'string');
    },

    notEmpty(value) {
        if (this.isSet(value) && value !== null) {
            if (this.isArray(value)) {
                return value.length > 0;
            } else if (this.isObject(value)) {
                return Object.keys(value).length > 0
            } else if (this.isTypeOf(value,'mumber') || this.isTypeOf(value,'bigint')) {
                return value !== 0;
            } else if (this.isString(value)) {
                return value !== '';
            } else if (this.isTypeOf(value,'boolean')) {
                return value;
            }

            return true;
        }

        return false;
    },

    empty(value) {
        return !this.notEmpty(value);
    },

    notExists(key, object) {
        return !this.exists(key, object);
    },

    exists(key, object) {
        return (key in object);
    },

    setDefault(value, defaultValue) {
        if (this.isSet(value)) {
            return value;
        }

        return defaultValue;
    },

    callCallback(callback, ...args) {
        if (typeof callback === "function") {
            callback(...args);
        }
    }
};