class XR_Webix_Widget {
    id;
    layoutConfig = {};
    viewConfig = null;
    container = null;
    params;
    events = {};
    app = {};
    viewSuffix = '';

    getViewConfig() {
        if (this.viewConfig === null) {
            return this.layoutConfig;
        }

        return this.viewConfig;
    }

    getLayout() {
        return $$(this.id);
    }

    getView(suffix) {
        if (XR.empty(suffix)) {
            if (XR.empty(this.viewSuffix)) {
                return this.getLayout();
            }

            return $$(this.id + this.viewSuffix);
        }

        return $$(this.id + '_' + suffix);
    }

    $$(suffix) {
        return this.getView(suffix);
    }

    $$$() {
        return this.getLayout();
    }

    show() {
        $$(this.id).show();
    }

    hide() {
        $$(this.id).hide();
    }

    isVisible() {
        $$(this.id).isVisible();
    }

    setValue(id, value) {}

    setFilter(id, value, reload, callback) {}

    affectLink(link, row, callback) {
        if (typeof row === 'object') {
            if ('row' in row) {
                row = this.$$().getItem(row.row);
            }
        } else {
            row = this.$$().getItem(row);
        }

        let linkWidget = XR.Webix.get(link.id);

        /*if (!linkWidget.isVisible()) {
            linkWidget.show();
        }*/
        if (link.from.indexOf(',') === -1, link.to.indexOf(',') === -1) {
            linkWidget.setValue(link.to, row[link.from], true);
            linkWidget.setFilter(link.to, row[link.from], true, callback);
        } else {
            let from = link.from.split(',');
            let to = link.to.split(',');

            for (let i in from) {
                linkWidget.setValue(to[i], row[from[i]], true);
                linkWidget.setFilter(to[i], row[from[i]], true, callback);
            }
        }
    }

    affectIDs(element) {
        if (XR.exists('affectIDs', element)) {
            if (XR.isArray(element.affectIDs)) {
                for (let i in element.affectIDs) {
                    if (XR.isFunction($$$(element.affectIDs[i]).load)) {
                        $$$(element.affectIDs[i]).load();
                    }
                }
            } else {
                if (XR.isFunction($$$(element.affectIDs).load)) {
                    $$$(element.affectIDs).load();
                }
            }
        }
    }

    prepareApp() {
        if (XR.exists('App', XR.Webix) && XR.exists(this.id, XR.Webix.App)) {
            this.app = XR.Webix.App[this.id];

            if (XR.exists('events', this.app)) {
                for (let event in this.app.events) {
                    this.attachEvent(event, this.app.events[event]);
                }
            }
        }
    }

    constructor(params) {
        this.id = params.id;

        if (XR.exists('container', params)) {
            this.container = params.container;

            if (params.container === null) {
                delete params.container;
            }
        }

        this.params = params;

        this.prepareApp();

        XR.Webix.add(this.id, this);
    }

    build() {
        webix.ui(this.config());

        XR.Webix.applyWidgetsBuildCallbacks();
    }

    config() {
        this.class = this.constructor.name

        this.layoutConfig.id = this.id;
        this.layoutConfig.hidden = this.params.hidden;
        this.layoutConfig.gravity = this.params.gravity;

        if (this.params.title !== null) {
            if ('rows' in this.layoutConfig) {
                this.layoutConfig.rows.unshift({
                    template: this.params.title,
                    type: "header"
                });
            } else if ('cols' in this.layoutConfig) {
                this.layoutConfig.rows = [
                    {
                        template: this.params.title,
                        type: "header"
                    },
                    {
                        cols: this.layoutConfig.cols
                    }
                ];

                delete this.layoutConfig.cols;
            } else {
                this.layoutConfig.title = this.params.title;
            }
        }

        if (this.container !== null) {
            this.layoutConfig.container = this.container;
        }

        return this.layoutConfig;
    }
}

Object.assign(XR_Webix_Widget.prototype, XR.Webix.mixins.Events);