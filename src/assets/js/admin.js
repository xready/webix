XR.Webix.Admin = {
    header(layout, title, menuId) {
        if (layout === 'tabbar') {
            return {
                view: "toolbar",
                elements: [
                    {
                        template: title,
                        type: "header",
                        borderless: true
                    },
                    {},
                    {
                        view: "icon",
                        icon: "mdi mdi-logout",
                        click: () => {
                            location.href = XR.Webix.baseUrl + 'auth/logout';
                        }
                    }
                ]
            }
        } else {
            return {
                view: "toolbar", elements: [
                    {
                        view: "icon",
                        icon: "mdi mdi-menu",
                        align: "left",
                        //css: "app_button",
                        click: () => {
                            $$$(menuId).$$().toggle();
                        }
                    },
                    {view: "label", label: title},
                    {},
                    //{ view: "icon", icon: "mdi mdi-logout",  badge:4}
                    {
                        view: "icon",
                        icon: "mdi mdi-logout",
                        click: () => {
                            location.href = XR.Webix.baseUrl + 'auth/logout';
                        }
                    }
                ]
            }
        }
    }
}