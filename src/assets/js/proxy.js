webix.proxy.xr = {
    $proxy:true,
    clearInvalid(view, id) {
        view.data.removeMark(id, "webix_invalid", true);

        for (var key in view.data.getMark(id, "$cellCss")) {
            view.removeCellCss(id, key, "webix_invalid_cell");
        }
    },
    addInvalid(view, id) {
        view.data.addMark(id, "webix_invalid", true);
    },
    addInvalidCell(view, id, key) {
        view.addCellCss(id, key, "webix_invalid_cell");
    },
    save(view, params, dp) {
        let method;
        let url;
        let data = params.data;

        if (params.operation === "insert" || (typeof params.data.id !== 'number' && params.data.id.indexOf('new') === 0)) {
            this.clearInvalid(view, params.data.id);

            url = XR.Webix.dataUrl + 'create?model=' + this.source;
            method = 'post';
        } else if (params.operation === "update") {
            this.clearInvalid(view, params.data.id);

            url = XR.Webix.dataUrl + params.data.id + '?model=' + this.source;
            method = 'put';
        } else if (params.operation === "delete") {
            url = XR.Webix.dataUrl + params.data.id + '?model=' + this.source;
            method = 'del';
            data = [];
        }

        return webix.ajax()[method](url, data, () => {
            if ('tableName' in view.config) {
                XR.Webix.reloadCollections(view.config.tableName);
            }
        }).fail((xhr) => {
            if (params.operation === 'delete') {
                if (xhr.status !== 204) {
                    switch (xhr.status) {
                        case 424:
                            webix.alert('Невозможно удалить, т.к. объект используется');
                            break;
                        default:
                            webix.alert('Removal error');
                            break;
                    }

                    $$$(view.config.xrId).reload();
                }
            } else {
                if (xhr.status === 422) {
                    this.addInvalid(view, params.data.id);

                    let errors = JSON.parse(xhr.response);
                    for (let errorsKey in errors) {
                        this.addInvalidCell(view, params.data.id, errors[errorsKey].field);

                        /*view.focusEditor({
                            row: params.data.id,
                            column: errors[errorsKey].field
                        });*/
                        //TODO: Focus on error field better for editor
                    }
                }
            }

        });
    },
    applyFilter(url, filter) {
        for (let field in filter) {
            if (filter[field] !== '') {
                url += (url.indexOf("?") !== -1 ? "&" : "?") + 'filter['+field+']='+filter[field];
            }
        }

        return url;
    },
    load(view, params) {
        let url = XR.Webix.dataUrl;
        let urlParams = {};

        let sourceSplit = this.source.split('/');

        let filter = {};

        if (sourceSplit.length > 1) {
            urlParams.model = sourceSplit[0];

            let sourceParams = {

            }

            for (let i = 1; i < sourceSplit.length; i += 2) {
                sourceParams[sourceSplit[i]] = sourceSplit[i+1];
            }

            for (let param in sourceParams) {
                switch (param) {
                    case 'tree':
                        url += 'tree/';
                        urlParams.field = sourceParams[param];
                        break;
                    case 'one':
                        url += 'one/';
                        urlParams.id = sourceParams[param];
                        break;
                    case 'combo':
                        url += 'combo/';
                        urlParams.relation = sourceParams[param];
                        break;
                    case 'combo_table':
                        url += 'combo_table/';
                        urlParams.table = sourceParams[param];
                        break;
                    case 'attribute':
                    case 'id':
                    case 'values':
                    case 'paging':
                        urlParams[param] = sourceParams[param];
                        break;
                    case 'filter':
                        Object.assign(filter, JSON.parse(sourceParams[param]));
                        break;
                }
            }
        } else {
            urlParams.model = this.source;
        }

        if (params !== null) {
            if (XR.isSet(params.start)) {
                urlParams.start = params.start;
            }

            if (XR.isSet(params.sort)) {
                urlParams.sort = params.sort.id;
                urlParams.sort_direction = params.sort.dir;
            }

            if (XR.isSet(params.filter)) {
                Object.assign(filter, params.filter);
            }
        }

        if (XR.notEmpty(filter)) {
            url = this.applyFilter(url, filter);
        }

        return webix.ajax().get(url, urlParams);
    }
};