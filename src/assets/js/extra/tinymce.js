webix.protoUI({
    name: "xr-tinymce",
    defaults:{
        config: {
            //theme: "silver",
            statusbar: false,
            plugins: 'code lists media link',
            menubar: false,
            toolbar: [ 'bold italic underline | bullist numlist | outdent indent | alignleft aligncenter alignright | link unlink media | code | undo redo' ]
        },
        template: (obj, common) => {
            let name = obj.name || obj.id;
            let id = "x" + webix.uid();

            let html = common._baseInputHTML("textarea") + "style='width:" + common._get_input_width(obj) + "px;' id='" + id + "' name='" + name + "'>" + common._pattern(obj.value) + "</textarea>";

            return common.$renderInput(obj, html, id);
        },
        labelPosition: 'top',
        value: ''
    },
    $init: function() {
        this._waitEditor = webix.promise.defer();
        this._waitLoad = webix.promise.defer();

        this.$ready.push(this._require_tinymce_once);
    },
    _require_tinymce_once: function(){
        let config = this.config;

        if (config.cdn === false || window.tinymce){
            this._init_tinymce_once();
            return;
        }

        let apiKey = config.apiKey ? config.apiKey : "no-api-key";
        let cdn = config.cdn || "https://cdn.tiny.cloud/1/"+apiKey+"/tinymce/5.0.14-54";

        //path to tinymce codebase
        window.tinyMCEPreInit = {
            query: "",
            base: cdn,
            suffix: ".min"
        };

        webix.require([cdn+"/tinymce.js"])
            .then( webix.bind(this._after_load, this) )
            .catch((e) => {
                console.log(e);
            });

    },
    _after_load: function () {
        this._waitLoad.resolve();
    },
    _init_tinymce_once: function(){
        if (!tinymce.dom.Event.domLoaded){
            // woraround event logic in tinymce
            tinymce.dom.Event.domLoaded = true;
        }

        if (this.config.config && this.config.tinyMCEToolbar) {
            this.config.config.toolbar = this.config.tinyMCEToolbar;
        }

        var editor_config = webix.copy(this.config.config || {});
        webix.extend(editor_config, {
            selector: "#"+this.getInputNode().id,
            resize: false
        }, true);

        let custom_setup = editor_config.setup;
        editor_config.setup = webix.bind((editor) => {
            if (custom_setup) {
                custom_setup(editor);
            }
            editor.on("init", webix.bind(this._mce_editor_ready, this), true);
        }, this);

        webix.delay(() => {
            tinymce.init(editor_config)
        }, this);
    },
    _mce_editor_ready: function(event){
        this._editor = event.target;

        this.setValue(this.config.value);
        this._set_inner_size();
        this._waitEditor.resolve(this._editor);
    },
    _reinit: function () {
        this._editor.destroy();
        delete this._editor;

        this._waitEditor = webix.promise.defer();
        this.render();
    },
    _set_inner_size: function(){
        if (this._is_editor()) {
            this.$view.querySelector(".tox-tinymce").style.height = (this.$height - 25) +"px";
        } else {
            this._waitLoad.then( webix.bind(this._init_tinymce_once, this) );
        }
    },
    _is_editor: function () {
        return (this._editor && this._editor.iframeElement.contentDocument !== null);
    },
    _is_reinit: function () {
        return (
            this._editor && (
                this._editor.iframeElement.contentDocument === null ||
                this._editor.iframeElement.contentDocument.body.id === ''
            )
        );
    },

    //Standard functions
    $setSize: function(x,y){
        if (webix.ui.textarea.prototype.$setSize.call(this, x, y)){
            this._set_inner_size();
        }
    },
    setValue: function(value) {
        if (value === null) {
            value = '';
        }

        if (this._is_reinit()) {
            this._reinit();
        }

        this.config.value = value;
        this._waitEditor.then((editor) => {
            editor.setContent(value);
        });
    },
    getValue: function(){
        return this._editor?this._editor.getContent():this.config.value;
    },
    focus: function(){
        this._waitEditor.then((editor) => {
            editor.focus();
        });
    }
}, webix.ui.textarea);