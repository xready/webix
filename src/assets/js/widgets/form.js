class XR_Webix_Form extends XR_Webix_Widget {
    dataWidget;
    create;
    update;
    formClass;

    bound = false;
    laterValues = {};

    uploads = [];

    newDataPrepend;
    formFields = [];
    clearAfterAdd = true;

    send() {
        this.$$().clearValidation();

        let data = this.$$().getValues();

        let ajaxParams = {
            error: function (text, data, xhr) {
                //XRE.DMI.screen.unlock();

                webix.alert({
                    type: "alert-error",
                    title: 'Error',
                    text: 'Error: ' + xhr.status + ' ' + xhr.statusText
                });
            },
            success: (text, data) => {
                //XRE.DMI.screen.unlock();

                var json = data.json();

                if (json) {
                    if (json.success) {
                        if (XR.exists('message', json)) {
                            webix.message(json.message);
                        }

                        if (this.clearAfterAdd) {
                            this.clear();
                        }
                    } else {
                        if (XR.exists('errors', json)) {
                            for (let field in json.errors) {
                                $$(this.id).markInvalid(field, json.errors[field][0]);
                            }
                        } else if (XR.exists('message', json)) {
                            webix.alert({
                                type: "alert-error",
                                title: 'Error',
                                text: json.message
                            });
                        }
                    }

                } else {
                    webix.alert({
                        type: "alert-error",
                        title: 'Error',
                        text: 'Error: ' + text
                    });
                }
            }
        };

        webix.ajax().post(XR.Webix.baseUrl + 'form/handle?form=' + this.formClass, data, ajaxParams);
    }

    ajaxValidate(success) {
        success();
    }

    save(upload) {
        upload = XR.setDefault(upload, 0);

        if (this.$$().validate()) {
            if (this.uploads.length > upload) {
                if (!$$(this.uploads[upload] + '_uploader').isUploaded()) {
                    $$(this.uploads[upload] + '_uploader').send((response) => {
                        if (response) {
                            if (response.success) {
                                $$(this.uploads[upload] + '_value').setValue(JSON.stringify(response.data));

                                this.save(upload + 1);
                            }
                        } else {
                            $$(this.uploads[upload] + '_value').setValue(null);

                            this.save(upload + 1);
                        }
                    });
                } else {
                    if (!$$(this.uploads[upload] + '_value').getValue().includes('uploaded')) {
                        $$(this.uploads[upload] + '_value').setValue(null);
                    }

                    this.save(upload + 1);
                }

            } else {
                this.ajaxValidate(() => {
                    this.send();
                })
            }
        }
    }

    afterAdd(id) {

    }

    clear() {
        let elements = this.$$().elements;

        let values = {}

        for (let name in elements) {
            let element = elements[name];

            if (element.config.clearAfterAdd) {
                if (element.config.defaultValue !== null) {
                    values[name] = element.config.defaultValue;
                } else {
                    values[name] = '';
                }

                if ('related' in element.config) {
                    $$(element.config.related).setValue('');
                }
            } else {
                values[name] = element.getValue();
            }

            if (element.config.focusAfterAdd) {
                this.$$().focus(name);
            }
        }

        this.$$().setValues(values, false);
    }

    prepareField(formField, params) {
        formField.id = this.id + '_fields_' + formField.name;

        if (XR.exists('relation', formField) && formField.relation !== null) {
            let collection = XR.Webix.getCollection(params.modelClass, formField.relation);

            collection.webix.attachEvent('onAfterLoad', () => {
                this.$$().elements[formField.name].refresh();
            });

            if (collection !== null) {
                formField.suggest = {
                    data: collection.webix
                }
            }
        }

        switch (formField.view) {
            case 'datepicker':
                formField.format = "%Y-%m-%d";
                if (formField.defaultValue === 'now') {
                    formField.defaultValue = new Date();
                }
                break;
            case 'file':
            case 'image':
                formField = this.prepareUploader(formField.view, formField, params);
                break;
            case 'xr-tinymce':
                formField.apiKey = XR.Webix.tinyMCEApiKey;
                formField.minHeight = 350;
                break;
            case 'textarea':
                formField.labelPosition = "top"
                break;
        }

        if (params.view !== 'toolbar' && formField.view !== 'checkbox' && (formField.label === '' || formField.label === null)) {
            formField.label = formField.placeholder;
        }

        if (params.labelWidth !== null) {
            formField.labelWidth = params.labelWidth;
        }

        this.formFields.push(formField);

        this.afterBuild(() => {
            if (formField.defaultValue !== null) {
                this.setValue(formField.name, formField.defaultValue);
            }

            let field = this.getField(formField.name);

            if (XR.notEmpty(field)) {
                webix.UIManager.addHotKey("Command+Enter", () => {
                    this.save();
                }, field);
            }
        });
    }

    prepareFields(params) {
        let fields = params.fields;

        for (let i in fields) {
            this.prepareField(fields[i], params);
        }
    }

    getField(name) {
        return $$(this.id + '_fields_' + name);
    }

    setValue(name, value) {
        if (this.bound) {
            let elements = this.$$().elements;

            if (name in elements) {
                elements[name].setValue(value);
            }
        } else {
            this.laterValues[name] = value;
        }
    }

    setLaterValues() {
        if (this.bound) {
            for (let id in this.laterValues) {
                this.setValue(id, this.laterValues[id]);
            }

            this.laterValues = {};
        }
    }

    setDefaultValues() {

    }

    constructor(params, rules) {
        super(params);

        this.formClass = params.formClass;
        this.dataWidget = params.dataWidget;
        this.update = params.update;
        this.create = params.create;

        this.newDataPrepend = params.newDataPrepend;

        this.clearAfterAdd = params.clearAfterAdd;

        this.prepareFields(params);

        let elementsKey = 'elements';

        if (params.view === 'toolbar') {
            elementsKey = 'cols';

            this.formFields.push({
                view: "icon",
                icon: 'mdi mdi-check',
                id: this.id + '_submit_button',
                click: () => {
                    this.save();
                }
            });
        } else {
            this.formFields.push({
                view: "button",
                value: params.saveButton,
                id: this.id + '_submit_button',
                click: () => {
                    this.save();
                }
            });
            this.formFields.push({
                view: "spacer"
            });
        }

        this.layoutConfig = {
            view: params.view,
            id: params.id + '_form',
            rules: {
                //$all: (value) => { return webix.rules.isNotEmpty(value); }
            }
        }

        //this.applyRules(rules,['onValues', 'onChange'], () => {
        //    return this.$$().elements;
        //});

        this.layoutConfig[elementsKey] = this.formFields;

        if (this.dataWidget !== null) {
            this.afterBuild(() => {
                this.$$().bind(this.getDataWidget().config.id);
                this.$$().attachEvent('onBindRequest', () => {
                    this.bound = true;
                    this.setLaterValues();
                });

                let dataStore = this.getDataWidget().data;
                dataStore.attachEvent('onIdChange', (oldId, newId) => {
                    this.fireEvent('onXrAdd', newId, dataStore.getItem(newId));
                    this.afterAdd(newId);
                })
            });
        } else {
            this.bound = true;
        }

        this.applyEvents();
    }
}

Object.assign(XR_Webix_Form.prototype, XR.Webix.mixins.Validation, XR.Webix.mixins.Uploader);