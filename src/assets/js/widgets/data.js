class XR_Webix_Data extends XR_Webix_Widget {
    header = [];
    filters = [];

    layoutConfig = {
        rows: []
    };

    footer = false;

    viewSuffix = '_data';

    model;
    tableName;
    columns = [];
    primaryKeys = [];

    buttons = [];
    rowButtons = [];

    edit;
    paging = true;
    pageSize = 20;
    create = true;

    filter = {};
    defaultFilters = {};
    loadUrl;
    loaded = false;

    childrenParams = {};
    childrenAfterAdd = false;

    collections = [];

    buildUrl() {
        let url = this.loadUrl;

        if (Object.keys(this.filter).length > 0) {
            url += '/filter/' + JSON.stringify(this.filter);
        }

        return url;
    }

    loadCollections(callback, stack) {
        stack = XR.setDefault(stack, 0);

        if (stack < this.collections.length) {
            XR.Webix.loadCollection(this.collections[stack], false, () => {
                this.loadCollections(callback, stack + 1);
            });
        } else {
            this.$$().load(this.buildUrl()).then(() => {
                this.checkFilters();
                XR.callCallback(callback, this);
            });
            this.loaded = true;
        }
    }

    load(force, callback) {
        force = XR.setDefault(force, false);

        if (force || !this.loaded) {
            this.loadCollections(callback);
        }
    }

    reload(callback) {
        if (this.loaded) {
            if (XR.exists('_filter_elements', this.$$())) {
                if (XR.notEmpty(this.$$()._filter_elements)) {
                    this.$$().filterByAll();
                } else {
                    this.$$().clearAll();
                    this.load(true, callback);
                }
            } else {
                this.load(true, callback);
            }
        } else {
            this.load(true, callback);
        }
    }

    setFilter(id, value, reload, callback) {
        let filter = null;

        if (XR.exists('getFilter', this.$$())) {
            filter = this.$$().getFilter(id);
        }

        if (filter !== null) {
            if (filter.value !== value) {
                filter.value = value;

                if (reload) {
                    this.reload(callback);
                } else {
                    XR.callCallback(callback, this)
                }
            }
        } else {
            if (this.getFilter(id) !== value) {
                this.filter[id] = value;

                if (reload) {
                    this.reload(callback);
                } else {
                    XR.callCallback(callback, this)
                }
            }
        }
    }

    isDefaultFilter() {
        return (Object.keys(this.defaultFilters).length > 0);
    }

    setDefaultFilter() {
        for (let id in this.defaultFilters) {
            let value = this.defaultFilters[id];

            this.setFilter(id, value, false);
        }
    }

    checkFilters() {
        if (XR.exists('getFilter', this.$$())) {
            if (XR.notEmpty(this.filter)) {
                for (let id in this.filter) {
                    let webixFilter = this.$$().getFilter(id);

                    if (webixFilter !== null) {
                        webixFilter.value = this.filter[id];
                        delete this.filter[id];
                    }
                }
            }
        }
    }

    getFilter(id) {
        if (XR.exists(id, this.filter)) {
            return this.filter[id];
        }

        return null;
    }

    setValue(id, value) {
        if (this.create) {
            let form = this.getForm();
            if (form) {
                form.setValue(id, value);
            }
        }
    }

    removeFilter(id) {
        if (XR.exists(id, this.filter)) {
            delete this.filter[id];
        }
    }

    clearFilter() {
        this.filter = {};
    }

    deleteRow(selection) {
        if (XR.isObject(selection)) {
            selection = selection.row;
        }

        this.$$().remove(selection);
    }

    getForm() {
        return XR.Webix.get(this.id + '_form');
    }

    prepareForm(params, inlineForm) {

    }

    attachForm(params, inlineForm) {
        if (typeof inlineForm !== "undefined" && inlineForm !== null) {
            if (params.formPosition === 'top') {
                this.layoutConfig.rows.unshift(inlineForm);
            } else {
                this.layoutConfig.rows.push(inlineForm);
            }
        }
    }

    prepareEdit(params, inlineForm, rules) {
        if (this.edit === 'inline') {
            if (params.insertMethod === 'inline' || params.insertMethod === 'multi' || params.updateMethod === 'multi' ) {
                this.prepareForm(params, inlineForm);
            }

            if (params.updateMethod === 'inline') {
                if (params.update) {
                    this.viewConfig.editable = true;
                    this.viewConfig.select = 'cell';
                    this.viewConfig.editaction = "custom";

                    this.attachEvent('onItemDblClick', () => {
                        this.startEdit();
                    });

                    this.afterBuild(() => {
                        webix.UIManager.addHotKey("Enter", () => {
                            this.startEdit();
                        }, this.$$());
                    });

                    this.applyRules(rules);
                }
            }
        }
    }

    prepareDelete(params) {
        if (params.delete) {
            this.addRowDeleteIcon();
        }
    }

    prepareLoad(params) {
        if (this.isDefaultFilter()) {
            this.afterBuild(() => {
                this.setDefaultFilter();
            });

            this.attachEvent('onViewShow', () => {
                if (this.loaded) { //Потому что вызываем checkFilters после load
                    this.checkFilters();
                }
            });
        }

        if (params.autoload) {
            this.afterBuild(() => {
                this.load();
            })
        }
    }

    prepareViewConfig(params) {
        this.viewConfig.xrId = this.id;
    }

    prepareChildren(params, childrenLayout) {
        for (let i in childrenLayout.rows[0].options) {
            let child = $$$(childrenLayout.rows[0].options[i].id);

            this.childrenParams[child.id] = {
                affectData: false,
                affectCallback: null,
                from: null,
                to: null,
                main: false
            }

            let childParams = this.childrenParams[child.id];

            if (XR.exists(child.id, params.childrenData)) {
                Object.assign(childParams, params.childrenData[child.id]);
            }

            let dataChild = child;

            if (XR.exists('dataId', childParams)) {
                dataChild = $$$(childParams.dataId);
            }

            if (dataChild instanceof XR_Webix_Data) {
                if (childParams.from !== null && childParams.to !== null) {
                    childParams.affectData = true;
                } else {
                    if (this.tableName === dataChild.tableName && this.primaryKeys.length === 1) {
                        childParams.affectData = true;
                        childParams.from = this.primaryKeys[0];
                        childParams.to = this.primaryKeys[0];
                    }

                    if (this.primaryKeys.length === 1) {
                        for (let k in dataChild.columns) {
                            if (dataChild.columns[k].relation !== null) {
                                let relation = dataChild.columns[k].relation;

                                if (relation.table === this.tableName) {
                                    childParams.affectData = true;
                                    childParams.from = this.primaryKeys[0];
                                    childParams.to = relation.attribute;
                                }
                            }
                        }
                    }

                    if (dataChild.primaryKeys.length === 1) {
                        for (let k in this.columns) {
                            if (this.columns[k].relation !== null) {
                                let relation = this.columns[k].relation;

                                if (relation.table === dataChild.tableName) {
                                    childParams.affectData = true;
                                    childParams.from = relation.attribute;
                                    childParams.to = dataChild.primaryKeys[0];
                                }
                            }
                        }
                    }
                }
            } else {
                delete childParams.dataId;
            }

            if (childParams.affectData) {
                childParams.dataId = dataChild.id;

                if (XR.isString(childParams.affectCallback) && XR.exists(childParams.affectCallback, this.app)) {
                    childParams.affectCallback = this.app[childParams.affectCallback];
                }
            }
        }
    }

    add() {
        if (this.create) {
            this.getForm().clear();

            if (this.edit === 'inline' && XR.exists('jumpToForm', this)) {
                this.jumpToForm();
            } else if (this.edit === 'child') {
                if (this.childrenAfterAdd) {
                    for (let childId in this.childrenParams) {
                        if (!this.childrenParams[childId].main) {
                            $$$(this.id + '_children').$$().hideOption(childId)
                        }
                    }
                }

                this.showChildren()
            }
        }
    }

    showChildren(selection) {
        $$(this.id + '_form_layout').show();

        if ($$$(this.id + '_children')) {
            let tabbar = $$$(this.id + '_children').$$();

            if (selection) {
                $$(this.id + '-details-title').setValue(XR.Webix.xtemplate(this.$$().getItem(selection.row), this.childrenTitle, true));
            } else {
                $$(this.id + '-details-title').setValue(this.childrenTitleNew);
            }

            for (let childId in this.childrenParams) {
                let childParams = this.childrenParams[childId];

                if (selection) {
                    if (childParams.affectData) {
                        this.affectLink({
                            id: childParams.dataId,
                            from: childParams.from,
                            to: childParams.to
                        }, selection, childParams.affectCallback);
                    }
                }

                if (selection || !this.childrenAfterAdd) {
                    tabbar.showOption(childId)
                }
            }

            if (this.childrenResetAfterOpen) {
                tabbar.setValue(tabbar.config.options[0].id);
            }
        }
    }

    constructor(params, form, childrenLayout, rules) {
        super(params);

        this.create                 = params.create;
        this.model                  = params.modelClass;
        this.tableName              = params.tableName;
        this.edit                   = params.edit;
        this.paging                 = params.paging;
        this.pageSize               = params.pageSize;
        this.childrenAfterAdd       = params.childrenAfterAdd;
        this.childrenResetAfterOpen = params.childrenResetAfterOpen;
        this.childrenTitle          = params.childrenTitle;
        this.childrenTitleNew       = params.childrenTitleNew;

        for (let i in params.columns) {
            let column = params.columns[i];

            if (column.table.mobileHidden && webix.env.mobile) {
                column.table.hidden = true;
            }

            this.columns.push(column);
        }

        this.primaryKeys = params.primaryKeys;

        this.loadUrl = 'xr->' + this.model;

        this.prepareViewConfig(params);

        let pagerConfig = null;

        if (this.paging) {
            this.viewConfig.pager = this.id + '_pager';

            pagerConfig = {
                view: "pager",
                id: this.id + '_pager',
                size: this.pageSize,
                group:5,
                template:" {common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
            }
        } else {
            this.loadUrl += '/paging/all'
        }

        this.prepareLoad(params);

        if (params.affectLinks !== null) {
            this.attachEvent('onAfterSelect', (widget, selection) => {
                if (XR.isObject(selection)) {
                    selection = selection.row;
                }

                let row = this.$$().getItem(selection);

                for (let i in params.affectLinks) {
                    this.affectLink(params.affectLinks[i], row);
                }
            })
        }

        this.prepareAppButtons();
        this.prepareEdit(params, form, rules);
        this.prepareDelete(params);

        if (this.edit === 'child') {
            this.attachEvent('onItemClick', (widget, selection) => {
                if (!this.selected) {
                    this.$$().unselect(selection.row);
                    this.$$().select(selection.row);
                }

                this.selected = false;
            });

            this.attachEvent('onAfterSelect', (widget, selection) => {
                this.selected = true;
                this.showChildren(selection);
            });
        }

        this.applyRowButtons();
        this.applyEvents();

        this.layoutConfig.rows.push(this.viewConfig);

        if (params.reloadButton) {
            if (!XR.isString(params.reloadButton)) {
                params.reloadButton = 'Reload';
            }

            this.addButton({
                title: params.reloadButton,
                handler: () => {
                    this.reload();
                }
            });
        }

        if (params.addButton) {
            if (!XR.isString(params.addButton)) {
                params.addButton = 'Add';
            }

            this.addButton({
                title: params.addButton,
                handler: () => {
                    this.add();
                }
            });
        }

        if (this.edit === 'inline') {
            this.attachForm(params, form);
        }

        this.applyButtons();

        if (pagerConfig !== null) {
            this.layoutConfig.rows.push(pagerConfig);
        }

        if (childrenLayout !== null) {
            this.prepareChildren(params, childrenLayout);
        }

        if (this.edit === 'child') {
            this.layoutConfig.id = this.id + '_data_layout'

            this.layoutConfig = new XR_Webix_Multiview({}, [
                this.layoutConfig,
                {
                    id: this.id + '_form_layout',
                    rows: [
                        {
                            view: "toolbar",
                            elements: [
                                {
                                    view: "icon",
                                    icon: "mdi mdi-arrow-left",
                                    align: "left",
                                    click: () => {
                                        $$(this.id + '_data_layout').show();
                                    }
                                },
                                {
                                    id: this.id + '-details-title',
                                    view: "label",
                                    label: this.childrenTitleNew
                                }
                            ]
                        },
                        (childrenLayout !== null)?childrenLayout:form,
                    ]
                }
            ]).config();
        }
    }
}

Object.assign(XR_Webix_Data.prototype, XR.Webix.mixins.Buttons, XR.Webix.mixins.Validation);