//TODO: Выводить FK по значение, id скрывать, а коллекция для editor (но как переправить из performer в performer_id)
class XR_Webix_Datatable extends XR_Webix_Data {
    startEdit() {
        let selectedCell = this.$$().getSelectedId();
        if (selectedCell) {
            this.$$().edit(selectedCell);
        }
    }

    jumpToForm() {
        let selected = this.$$().getSelectedId();

        if (selected) {
            this.getForm().$$().focus(selected.column);
        } else {
            let children = this.getForm().$$().getChildViews(); //TODO: Заменить на elements

            if (children.length > 0) {
                children[0].focus();
            }
        }
    }

    prepareForm(params, inlineForm) {
        super.prepareForm(params, inlineForm)

        if (typeof inlineForm !== "undefined" && inlineForm !== null) {
            this.afterBuild(() => {
                webix.UIManager.addHotKey("Ctrl+I", () => {
                    this.jumpToForm();
                }, this.$$());

                webix.UIManager.addHotKey("Command+I", () => {
                    this.jumpToForm();
                }, this.$$());
            });

            for (let i in inlineForm.cols) {
                let formField = inlineForm.cols[i];

                if (!('on' in formField)) {
                    formField.on = {};
                }

                formField.on.onKeyPress = (code, event) => {
                    if (event.key === 'ArrowDown') {
                        if (formField) {
                            let id = this.$$().getFirstId();

                            webix.UIManager.setFocus(this.id + this.viewSuffix);
                            this.$$().select(id, formField.name);
                        }
                    }
                }
            }

            this.attachEvent('onKeyPress', (widget, code, event) => {
                if (event.key === 'ArrowUp') {
                    if (!this.$$().getPrevId(this.$$().getSelectedId().row)) {
                        this.jumpToForm();
                    }
                }
            });

            this.attachEvent('onAfterRender', () => {
                XR.Webix.get(this.id + '_form').adjustToTable();
            });

            this.attachEvent('onViewShow', () => {
                XR.Webix.get(this.id + '_form').adjustToTable();
            });

            this.attachEvent('onColumnResize', () => {
                XR.Webix.get(this.id + '_form').adjustToTable();
            });
        }
    }

    prepareColumns() {
        let tableColumns = [];

        for (let i in this.columns) {
            let column = this.columns[i];

            let tableColumn = column.table;

            if (tableColumn.mobileHidden && webix.env.mobile) {
                tableColumn.hidden = true;
            }

            if (tableColumn.template === null) {
                switch (column.type) {
                    case 'image':
                        tableColumn.template = (row, template, value, column) => {
                            if (row[tableColumn.id] !== null && row[tableColumn.id] !== '' && row[tableColumn.id].includes('base64')) {
                                return '<img src="'+row[tableColumn.id]+'" style="height: inherit"/>'
                            } else {
                                return '';
                            }
                        }
                        break;
                    case 'boolean':
                        if (!column.editable) {
                            tableColumn.template = (row, common, value, column) => {
                                if (value) {
                                    return '<div class="webix_icon mdi mdi-check"></div>';
                                }

                                return '';
                            }
                        }
                        break;
                    default:
                        if (
                            typeof tableColumn.xtemplate === 'string' ||
                            typeof tableColumn.xtemplate === 'boolean'
                        ) {
                            tableColumn.template = (row, common, value, column) => {
                                return XR.Webix.xtemplate(row, tableColumn.xtemplate, value, column, common)
                            }
                        } else if (typeof tableColumn.xtemplate === 'object') {

                        }
                }
            }

            if (column.relation !== null) {
                let collection = XR.Webix.getCollection(this.model, column.relation);

                if (collection !== null) {
                    this.collections.push(collection);

                    tableColumn.collection = collection.webix;
                }
            }

            if (column.total) {
                this.paging = false;
                this.footer = true;
                tableColumn.footer = {content: "summColumn" };
            } else if (column.footer) {
                this.footer = true;
            }

            if (column.defaultFilter) {
                this.defaultFilters[tableColumn.id] = column.defaultFilter;
            }

            if (tableColumn.extra) {
                Object.assign(tableColumn, tableColumn.extra);
            }

            tableColumns.push(tableColumn);
        }

        return tableColumns;
    }

    prepareViewConfig(params) {
        let columns = this.prepareColumns();

        this.viewConfig = {
            id: this.id + this.viewSuffix,
            tableName: params.tableName,
            view: "datatable",
            headermenu: true,
            //header: false,
            footer: this.footer,
            navigation: true,
            autoConfig: false,
            columns: columns,
            hover: 'xr_webix_row_hover',
            select: 'row',
            save: {
                updateFromResponse: true,
                url: 'xr->' + this.model
            },
            //autowidth: true,
            rules: {
                //$all: (value) => { return webix.rules.isNotEmpty(value); }
            }
        };

        Object.assign(this.viewConfig, params.extra);

        this.attachEvent('onBeforeLoad', () => {
            this.$$().showOverlay("Loading...");
        });

        this.attachEvent('onAfterLoad', () => {
            this.$$().hideOverlay();
        });

        super.prepareViewConfig(params);
    }
}

