class XR_Webix_Auth extends XR_Webix_Widget {
    auth() {
        $$(this.id).clearValidation();

        let data = $$(this.id).getValues();

        let ajaxParams = {
            error: function (text, data, xhr) {
                //XRE.DMI.screen.unlock();

                webix.alert({
                    type: "alert-error",
                    title: 'Error',
                    text: 'Error: ' + xhr.status + ' ' + xhr.statusText
                });
            },
            success: (text, data) => {
                //XRE.DMI.screen.unlock();

                var json = data.json();

                if (json) {
                    if (json.success) {
                        location.reload();
                    } else {
                        if (typeof json.errors !== 'undefined') {
                            for (let field in json.errors) {
                                $$(this.id).markInvalid(field, json.errors[field][0]);
                            }
                        }
                    }

                } else {
                    webix.alert({
                        type: "alert-error",
                        title: 'Error',
                        text: 'Error: ' + text
                    });
                }
            }
        };

        webix.ajax().post(XR.Webix.baseUrl + 'auth/login', data, ajaxParams);
    }

    constructor(params) {
        super(params);

        this.layoutConfig = {
            view: "form",
            elements:[{
                view:  "text",
                label: "Username",
                validate: webix.rules.isNotEmpty,
                invalidMessage:"Login can not be empty",
                name:  "username",
                id: this.id + '_username'
            }, {
                view:  "text",
                type:  "password",
                label: "Password",
                name:  "password",
                id: this.id + '_password'
            }, {
                view:  "checkbox",
                label: "Remember",
                name:  "remember",
                id: this.id + '_remember'
            }, {
                cols:[
                    {
                        view:  "button",
                        value: "Login",
                        type:  "form",
                        click: () => {
                            this.auth();
                        }
                    }, {
                        view:  "button",
                        value: "Cancel",
                        click: () => {
                            $$(this.id).clear();
                        }
                    }
                ]
            }]
        }

        webix.UIManager.addHotKey("Enter", () => {
            this.auth();
        }, $$(this.id + '_username'));

        webix.UIManager.addHotKey("Enter", () => {
            this.auth();
        }, $$(this.id + '_password'));
    }
}