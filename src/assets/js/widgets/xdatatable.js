//TODO: Выводить FK по значение, id скрывать, а коллекция для editor (но как переправить из performer в performer_id)
webix.protoUI({
    name: "xr-multicombo"
}, webix.ui.combo);

class XR_Webix_Xdatatable extends XR_Webix_Datatable {
    prepareColumns() {
        for (let i in this.columns) {
            let tableColumn = this.columns[i].table;

            if (!tableColumn.hidden) {
                this.header.push({
                    id: this.id + '_header_' + tableColumn.id,
                    view: "label",
                    //align: "center",
                    label: (XR.isArray(tableColumn.header)) ? tableColumn.header[0] : tableColumn.header
                });

                let formField = this.columns[i].form;

                if (formField.view === 'combo') {
                    formField.view = 'xr-multicombo';
                }

                if (XR.exists('relation', this.columns[i]) && this.columns[i].relation !== null) {
                    let collection = XR.Webix.getCollection(this.model, this.columns[i].relation);

                    collection.webix.attachEvent('onAfterLoad', () => {
                        this.$$().elements[formField.name].refresh();
                    });

                    if (collection !== null) {
                        formField.suggest = {
                            data: collection
                        }
                    }
                }

                this.filters.push(formField);
                //tableColumn.header = false;
            }
        }

        return super.prepareColumns();
    }

    prepareViewConfig(params) {
        super.prepareViewConfig(params);

        //this.viewConfig.header = false;

        this.layoutConfig.rows.push({
            view: 'toolbar',
            cols: this.header,
            css: 'xr_webix_datatable_header'
        });

        this.layoutConfig.rows.push({
            view: 'toolbar',
            cols: this.filters,
            css: 'xr_webix_datatable_header'
        });

        this.attachEvent('onAfterRender', () => {
            //console.log('onAfterRender');
            //this.adjustHeader();
        });

        this.attachEvent('onViewShow', () => {
            //console.log('onViewShow');
            //this.adjustHeader();
        });

        this.attachEvent('onResize', () => {
            this.adjustHeader();
        });

        this.attachEvent('onColumnResize', (view, column, width, old) => {
            //console.log('onColumnResize', column, width, old);
            //this.adjustColumnHeader(column);
        });
    }

    adjustColumnHeader(column) {
        if (XR.isString(column)) {
            column = this.$$().getColumnConfig(column);
        }

        let header = $$(this.id + '_header_' + column.id);

        if (header && !column.hidden) {
            header.config.width = column.width - 4; //Margin of label

            $$(this.id + '_header_' + column.id).resize();
        }
    }

    adjustHeader() {
        let columns = this.$$().getColumns();

        let resize = [];

        for (let i in columns) {
            let column = columns[i];

            let header = $$(this.id + '_header_' + column.id);

            if (header && !column.hidden) {
                header.config.width = column.width - 4; //Margin of label
                resize.push(header);
            }
        }

        for (let i in resize) {
            resize[i].resize();
        }
    }
}

