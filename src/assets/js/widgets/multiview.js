class XR_Webix_Multiview extends XR_Webix_Widget {
    constructor(params, cells) {
        super(params);

        if (cells.length !== 0) {
            this.layoutConfig.cells = cells;
        }
    }
}