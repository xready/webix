class XR_Webix_Layout extends XR_Webix_Widget {
    affectIDs = [];

    load() {
        for (let i in this.affectIDs) {
            if (XR.isFunction($$$(this.affectIDs[i]).load)) {
                $$$(this.affectIDs[i]).load();
            }
        }
    }

    constructor(params, rows, cols) {
        super(params);

        if (rows.length !== 0) {
            this.layoutConfig.rows = rows;

            for (let i in rows) {
                this.affectIDs.push(rows[i].id);
            }
        }

        if (cols.length !== 0) {
            this.layoutConfig.cols = cols;

            for (let i in cols) {
                this.affectIDs.push(cols[i].id);
            }
        }
    }
}