class XR_Webix_Dataform extends XR_Webix_Form {
    getDataWidget() {
        return $$$(this.dataWidget).$$();
    }

    send() {
        let values = this.$$().getValues();

        if ('id' in values && this.update) { //ugly костыль плохо
            this.getDataWidget().setBindData(values, this.getDataWidget().config.id);
            for (let i in this.uploads) {
                $$(this.uploads[i] + '_uploader').setValue(null);
            }
        } else if (this.create) {
            values.id = 'new-'+(new Date()).getTime();
            this.getDataWidget().data.add(values, (this.newDataPrepend)?0:undefined);
        }
    }

    ajaxValidate(success) {
        //super.ajaxValidate(success);
        this.$$().clearValidation();

        let data = this.$$().getValues();

        let ajaxParams = {
            error: function (text, data, xhr) {
                //XRE.DMI.screen.unlock();

                webix.alert({
                    type: "alert-error",
                    title: 'Error',
                    text: 'Error: ' + xhr.status + ' ' + xhr.statusText
                });
            },
            success: (text, data) => {
                //XRE.DMI.screen.unlock();

                var json = data.json();

                if (json) {
                    if (json.success) {
                        if (XR.exists('message', json)) {
                            webix.message(json.message);
                        }

                        success();
                    } else {
                        if (XR.exists('errors', json)) {
                            for (let field in json.errors) {
                                $$(this.id).markInvalid(field, json.errors[field][0]);
                            }
                        } else if (XR.exists('message', json)) {
                            webix.alert({
                                type: "alert-error",
                                title: 'Error',
                                text: json.message
                            });
                        }
                    }
                } else {
                    webix.alert({
                        type: "alert-error",
                        title: 'Error',
                        text: 'Error: ' + text
                    });
                }
            }
        };

        let url = XR.Webix.baseUrl + 'data/validate?model=' + this.formClass;

        if (XR.exists('id', data)) {
            url += '&id=' + data.id;
        }

        webix.ajax().post(url, data, ajaxParams);
    }

    afterAdd(id) {
        if (this.clearAfterAdd) {
            this.clear();
        } else {
            this.getDataWidget().select(id);
        }
    }

    constructor(params, rules) {
        let columns = params.columns;

        params.fields = [];

        for (let i in columns) {
            let column = columns[i];

            if ((params.view === 'toolbar' && !column.table.hidden) || column.editable) {
                column.form.relation = column.relation;

                params.fields.push(column.form)
            }
        }

        params.formClass = params.modelClass;

        super(params, rules);
    }

    adjustToTable() {
        let dataWidget = this.getDataWidget();
        let columns = this.getDataWidget().getColumns();
        let buttonWidth = 0;

        for (let i in columns) {
            let column = columns[i];
            let header = dataWidget.getHeaderNode(column.id);

            if (header) {
                let field = this.getField(column.id);

                if (field) {
                    if (column.fillspace) {
                        field.config.width = false;
                    } else {
                        field.config.width = header.clientWidth;
                    }
                } else {
                    buttonWidth += header.clientWidth;
                }
            }
        }

        if (buttonWidth !== 0) {
            $$(this.id + '_submit_button').config.width = buttonWidth;
        }
    }
}