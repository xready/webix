class XR_Webix_Template extends XR_Webix_Widget {
    constructor(params) {
        super(params);

        if (this.params.src === null) {
            delete this.params.src;
        }

        this.layoutConfig = this.params;
        this.layoutConfig.view = "template";
        //role: "placeholder"
    }
}