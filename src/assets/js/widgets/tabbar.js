class XR_Webix_Tabbar extends XR_Webix_Widget {
    viewSuffix = '_tabbar';

    collectAffectIds(widgets) {
        for (let i in widgets) {
            let element = this.$$().getOption(widgets[i].id);

            if (element) {
                element.affectIDs = widgets[i].id;
            }
        }
    }

    constructor(params, widgets) {
        super(params);

        let data = params.data;

        this.layoutConfig = {
            rows: [
                {
                    id: this.id + this.viewSuffix,
                    view: "tabbar",
                    multiview: true,
                    on: {
                        onAfterRender: () => {
                            this.collectAffectIds(widgets);
                            this.affectIDs(this.$$().getOption(this.$$().getValue()));
                        },
                        onChange: (id) => {
                            this.affectIDs(this.$$().getOption(id));
                        }
                    },
                    options: data
                },
                {
                    id: this.id + "_multiview",
                    cells: widgets
                }
            ]
        }

        this.afterBuild(() => {
            this.collectAffectIds(widgets);
        });
    }
}