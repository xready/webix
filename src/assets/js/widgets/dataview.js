//TODO: Выводить FK по значение, id скрывать, а коллекция для editor (но как переправить из performer в performer_id)
class XR_Webix_Dataview extends XR_Webix_Data {
    showColumnTitle;

    imageTemplate(image) {
        let template = '';

        if (image !== null) {


            template += '<div class="xr_cell xr_webix_dataview_cell_image">';

            if (image.value !== null && image.value !== '' && image.value.includes('base64')) {
                template += '<img src="'+image.value+'"' +image.height+ ' alt=""/>'
            }

            template += '</div>';
        }

        return template;
    }

    tableTemplate(data) {
        let template = '';

        template += '<div class="xr_cell xr_webix_dataview_cell_table">';
        template += '<div class="xr_table">';

        for (let i in data) {
            let item = data[i];

            template += '<div class="xr_row">';

            if (this.showColumnTitle) {
                template +=  '<div class="xr_cell xr_header">'+item.header+'</div>';
            }
            template +=  '<div class="xr_cell xr_value" title="'+item.title+'"' + item.width + '>'+item.value+'</div>';

            template +=  '</div>';
        }

        template += '</div>';
        template += '</div>';

        return template;
    }

    template(rowData, webix) {
        let templateWidth = '';
        let valueWidth = '';

        if (XR.isSet(webix)) {
            //Ширина всей ячейки Dataview, 24 - это стандартный padding в Dataview
            templateWidth = ' style="width: ' + (webix.width - 24) + 'px; max-width: ' + (webix.width - 24) + 'px;" ';

            //Ширина столбца со значениями, 24 - это стандартный padding в Dataview, 60 - ширина блока с кнопками
            let initWidth = webix.width - 24;

            if (this.rowButtons.length > 0) {
                initWidth -= 60;
            }

            if (this.showColumnTitle) {
                initWidth = initWidth/2
            }

            valueWidth = ' style="max-width: ' + initWidth + 'px;"';

        }

        let tableData = [];
        let imageData = null;

        for (let i in this.columns) {
            let column = this.columns[i];

            if (!column.table.hidden) {
                if (column.type === 'image' && imageData === null) {
                    imageData = {
                        value: (typeof webix === "undefined" || !(column.table.id in rowData))?'':rowData[column.table.id],
                        height: ''
                    }

                    if (typeof webix !== "undefined") {
                        imageData.height = ' style="height: ' + (webix.height - 6) +  'px;"';
                    }
                } else {
                    let header = column.table.header;

                    if (typeof header === "object") {
                        header = header[0];
                    }

                    header += ': ';

                    let value;

                    if (typeof webix === "undefined") {
                        value = header;
                    } else {
                        value = rowData[column.table.id];

                        if (value !== null && value !== '') {
                            if (column.relation !== null) {
                                let collection = XR.Webix.getCollection(this.model, column.relation, this.id + '_view');

                                if (collection !== null) {
                                    let item = collection.getItem(value);

                                    if (item) {
                                        value = collection.getItem(value).value;
                                    }
                                }
                            }

                            if (column.table.xtemplate !== null) {
                                column.table.template = column.table.xtemplate;
                            }

                            if (column.table.template !== null) {
                                if (typeof column.table.template === 'function') {
                                    value = column.table.template(rowData, undefined, value, column.table.id);
                                } else {
                                    value = XR.Webix.xtemplate(rowData, column.table.template, value, column.table.id);
                                }
                            }
                        } else {
                            value = '';
                        }
                    }

                    let title = value;

                    if (typeof title === "string") {
                        title = title.replace('"', '\'\'');
                    }

                    tableData.push({
                        header: header,
                        title: title,
                        value: value,
                        width: valueWidth
                    });
                }
            }
        }

        let template = '<div class="xr_table"' + templateWidth + '>';
        template += '<div class="xr_row">';

        template += this.imageTemplate(imageData);
        template += this.tableTemplate(tableData);

        if (this.rowButtons.length > 0) {
            if (webix) {
                template += '<div class="xr_cell xr_webix_dataview_cell_buttons">'
                for (let i in this.rowButtons) {
                    let button = this.rowButtons[i];
                    if (typeof button.template == "function") {
                        template += button.template(rowData);
                    } else {
                        template += button.template;
                    }
                }
                template += '</div>'
            } else {
                template += '<div class="xr_cell xr_webix_dataview_cell_buttons"></div>'
            }
        }

        template += '</div>';
        template += '</div>';

        return template;
    }

    prepareViewConfig(params) {
        this.viewConfig = {
            id: this.id + this.viewSuffix,
            view: "dataview",
            navigation: true,
            template: (...args) => {
                return this.template(...args);
            },
            save: {
                updateFromResponse: true,
                url: 'xr->' + this.model
            },
            //autowidth: true,
            pager: this.id + '_pager',
            //autoheight: true,
            sizeToContent: true,
            select: true,
            type: {
                //dimensions of each dataview item
                //height: height
                css: 'xr_webix_dataview_cell'
            },
            css: 'xr_dataview'
        };

        super.prepareViewConfig(params);
    }

    constructor(params, form, childrenLayout, rules) {
        super(params, form, childrenLayout, rules);

        this.showColumnTitle = params.showColumnTitle;
    }
}

