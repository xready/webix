class XR_Webix_Tree extends XR_Webix_Data {
    prepareViewConfig(params) {
        this.viewConfig = {
            id: this.id + this.viewSuffix,
            view: "tree",
            scheme: {
                $serialize: (row) => {
                    console.log(row);
                    return { id: row.id, value: row.title}
                },
            },
            /*navigation: true,
            template: (...args) => {
                return this.template(...args);
            },
            save: {
                updateFromResponse: true,
                url: 'xr->' + this.model
            },
            //autowidth: true,
            pager: this.id + '_pager',
            //autoheight: true,
            sizeToContent: true,
            select: true,
            type: {
                //dimensions of each dataview item
                //height: height
                css: 'xr_webix_dataview_cell'
            },
            css: 'xr_dataview'*/
        };

        super.prepareViewConfig(params);
    }

    constructor(params, form, childrenLayout, rules) {
        super(params, form, childrenLayout, rules);

        this.loadUrl += '/tree/'+params.valueField;
    }
}