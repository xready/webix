//TODO: Выводить FK по значение, id скрывать, а коллекция для editor (но как переправить из performer в performer_id)
class XR_Webix_Gridone extends XR_Webix_Data {
    rowId;

    buildUrl() {
        return this.loadUrl + '/one/' + this.rowId;
    }

    setFilter(id, value, reload, callback) {
        if (id === 'id') {
            this.rowId = value;

            if (reload) {
                this.reload(callback);
            } else {
                XR.callCallback(callback, this)
            }
        }
    }

    affectLink(link, callback) {
        let fromValue = this.$$().getItem(link.from).value;

        let linkWidget = XR.Webix.get(link.id);

        if (!linkWidget.isVisible()) {
            linkWidget.show();
        }

        linkWidget.setValue(link.to, fromValue, true);
        linkWidget.setFilter(link.to, fromValue, true, callback);
    }

    prepareViewConfig(params) {
        this.viewConfig = {
            id: this.id + this.viewSuffix,
            view: "datatable",
            header: false,
            navigation: true,
            select: 'row',
            columns: [
                {
                    id: 'label',
                    adjust: true
                },
                {
                    id: 'value',
                    fillspace: true
                }
            ]
        };

        super.prepareViewConfig(params);
    }
}