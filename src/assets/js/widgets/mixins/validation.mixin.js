XR.Webix.mixins.Validation = {
    clearValidation() {
        this.$$().clearValidation();
    },

    applyRules(rules, clearEvents, elementsFn) {
        if (typeof clearEvents === 'undefined') clearEvents = [];
        if (typeof elementsFn === 'undefined') elementsFn = null;

        if (XR.isSet(rules) && Object.keys(rules).length > 0) {
            clearEvents.forEach((eventName) => {
                this.attachEvent(eventName, () => {
                    this.clearValidation();
                });
            });

            for (let field in rules) {
                let rule = rules[field];

                if (XR.notExists('rules', this.getViewConfig())) {
                    this.getViewConfig().rules = {};
                }

                this.getViewConfig().rules[field] = (value, data, column) => {
                    if (value === 'NULL') value = '';

                    let messages = [];

                    rule(value, messages);

                    if (messages.length > 0) {
                        let message = messages.join('<br/>');

                        if (elementsFn !== null) {
                            let elements = elementsFn();

                            if (field in elements) {
                                elements[field].config.invalidMessage = message;
                            } else {
                                webix.message(message, 'error');
                            }
                        } else {
                            webix.message(message, 'error');
                        }

                        return false;
                    }

                    return true;
                }
            }
        }
    }
}