XR.Webix.mixins.Buttons = {
    rowIconTemplate(id, icon) {
        return '<div class="webix_icon '+icon+' xr_webix_row_button_icon ' + id + '"></div>';
    },

    rowButtonTemplate(id, title) {
        return '<div class="webix_view webix_control webix_el_button webix_secondary xr_webix_row_button ' + id + '""><div class="webix_el_box"><button type="button" class="webix_button">' + title + '</button></div></div>'
    },

    addRowIcon(icon, handler) {
        let id = 'xr_webix_row_button_icon_' + this.rowButtons.length;

        let button = {
            id: id,
            type: 'icon',
            //Вариант с webix кнопкой (также в css) template: '<div class="xr_webix_row_button_icon"><button type="button" class="webix_icon_button ' + id + '"><span class="webix_icon '+icon+'"></span></button></div>',
            handler: handler
        };

        if (typeof icon === "function") {
            button.template = (row) => {
                return icon(row, button);
            };
        } else {
            button.template = this.rowIconTemplate(id, icon);
        }

        this.rowButtons.push(button);
    },

    addRowDeleteIcon() {
        this.addRowIcon('mdi mdi-dots-horizontal', (widget, event, selection, ...args) => {
            let menu = $$(this.id + "_contextmenu");

            if (!menu.config.hidden && menu.$context.id === selection.row) {
                menu.hide(event.target);
            } else {
                menu.show(event.target);
                menu.$context = { widget: this, selection: selection };
            }
        });

            webix.ui({
                view: "contextmenu",
                id: this.id + "_contextmenu",
                data: [
                    //{value:"Add", icon: "mdi mdi-delete"},
                    {id: 'remove', value:"Remove", icon: "mdi mdi-delete"},
                    //"Info"
                ],
                click: function(id) {
                    let context = this.$context;

                    if (id === "remove") {
                        context.widget.deleteRow(context.selection);
                    }
                }
            });
    },

    addRowButton(title, handler) {
        let id = 'xr_webix_row_button_'+this.rowButtons.length;

        this.rowButtons.push({
            id: id,
            type: 'button',
            template: (typeof title === "function")?title:this.rowButtonTemplate(id, title),
            width: (title.length * 12) + 40,
            handler: handler
        });
    },

    applyRowButtonsToDataview() {
        let config = this.getViewConfig();

        if (this.rowButtons.length > 0) {
            config.onClick = {};

            for (let i in this.rowButtons) {
                let button = this.rowButtons[i];

                config.onClick[button.id] = (...args) => {
                    button.handler(this, ...args);
                    return false;
                };
            }
        }
    },

    applyRowButtons() {
        let config = this.getViewConfig();

        if (this.rowButtons.length > 0) {
            config.onClick = {};

            for (let i in this.rowButtons) {
                let button = this.rowButtons[i];

                if (XR.exists('columns', config)) {
                    let columnConfig = {
                        id: button.id,
                        header: '',
                        template: button.template
                    }

                    switch (button.type) {
                        case 'icon':
                            //css = 'xr_webix_row_button_icon_cell';
                            columnConfig.width = 60;
                            break;
                        case 'button':
                            columnConfig.css = 'xr_webix_row_button_cell';
                            //columnConfig.adjust = 'data';
                            columnConfig.width = button.width;
                            break;
                    }

                    config.columns.push(columnConfig)
                }

                config.onClick[button.id] = (...args) => {
                    button.handler(this, ...args);
                    return false;
                };
            }
        }
    },

    addButton(button) {
        let buttonId = this.id + '_buttons_' + this.buttons.length

        if ('view' in button) {
            if (!('id' in button)) {
                button.id = buttonId;
            }
            this.buttons.push(button);
        } else if ('title' in button && 'handler' in button) {
            this.buttons.push({
                id: buttonId,
                view: "button",
                value: button.title,
                click: () => {
                    button.handler(this);
                }
            });
        }

    },

    applyButtons() {
        if (this.buttons.length > 0) {
            this.layoutConfig.rows.push({
                cols: this.buttons
            });
        }
    },

    prepareAppButtons() {
        if ('rowIcons' in this.app) {
            for (let i in this.app.rowIcons) {
                this.addRowIcon(this.app.rowIcons[i].icon, this.app.rowIcons[i].handler);
            }
        }

        if ('rowButtons' in this.app) {
            for (let i in this.app.rowButtons) {
                this.addRowButton(this.app.rowButtons[i].title, this.app.rowButtons[i].handler);
            }
        }

        if ('buttons' in this.app) {
            for (let i in this.app.buttons) {
                this.addButton(this.app.buttons[i]);
            }
        }
    }
}