XR.Webix.mixins.Uploader = {
    showImage(layoutId, value) {
        let templateView = $$(layoutId + '_template');
        if (templateView) {
            if (value !== null && value.includes('base64')) {
                $$(layoutId + '_template').show();

                let image = $('#'+layoutId+'_image');

                if (image.length > 0) {
                    image[0].src = value;

                    if (image[0].complete) {
                        this.resizeImage(layoutId, image);
                    } else {
                        image.on('load', () => {
                            this.resizeImage(layoutId, image);
                        });
                    }
                }
            } else {
                $$(layoutId + '_template').hide();
            }
        }
    },

    clearImage(layoutId) {
        let templateView = $$(layoutId + '_template');

        if (templateView) {
            let image = $('#'+layoutId+'_image');

            if (image.length > 0) {
                image[0].src = '';
            }

            $$(layoutId + '_template').hide();
        }
    },

    clearUploader(layoutId) {
        $$(layoutId + '_uploader').files.clearAll();
        $$(layoutId + '_clear_button').hide();
    },

    resizeImage(layoutId, image) {
        let imageWidth = image.width();

        let viewWidth = imageWidth + 17;

        $$(layoutId + '_template').define("width", viewWidth);
        $$(layoutId + '_template').resize();
    },

    prepareUploader(type, formField, params) {
        this.uploads.push(formField.id);

        formField.view = 'text';

        let layoutId = formField.id;

        let label = {
            view: 'label',
            label: formField.placeholder,
            width: 80
        }

        let uploadButton = {
            view: "icon",
            icon: "mdi mdi-upload",
            //align: "left",
            click: () => {
                $$(layoutId + '_uploader').fileDialog();
            }
        }

        let clearButton = {
            id: layoutId + '_clear_button',
            view: "icon",
            icon: "mdi mdi-undo",
            hidden: true,
            click: () => {
                if (type === 'images') {
                    let values = this.$$().getValues();
                    if (XR.exists(formField.name, values) && values[formField.name] !== '' && values[formField.name] !== '') {
                        this.showImage(layoutId, values[formField.name]);
                    } else {
                        this.clearImage(layoutId);
                    }
                }

                this.clearUploader(layoutId);
            }
        }

        let deleteButton = {
            view: "icon",
            icon: "mdi mdi-delete",
            hidden: true,
            click: () => {

            }
        }

        if (type === 'image') {
            webix.ui({
                view: 'uploader',
                id: layoutId + '_uploader',
                multiple: false,
                autosend: false,
                upload: '/admin/upload/image',
                apiOnly: true,
                on: {
                    onAfterFileAdd: (upload) => {
                        let reader = new FileReader();
                        reader.onload = (event) => {
                            this.showImage(layoutId, event.target.result);
                        };
                        reader.readAsDataURL(upload.file);

                        $$(layoutId + '_clear_button').show();
                    }
                }
            });

            let template = {
                view: 'template',
                id: layoutId + '_template',
                hidden: true,
                template: () => {
                    return '<img id="'+layoutId+'_image" src="" alt="" style="height: inherit"/>';
                },
                borderless: true,
                type: 'clean'
            }

            formField.id = layoutId + '_value';
            formField.hidden = true;
            formField.related = layoutId + '_uploader';
            formField.on = {
                onChange: (value) => {
                    this.clearUploader(layoutId);
                    if (!value.includes('uploaded')) {
                        this.showImage(layoutId, value);
                    }
                }
            }

            let layout = {
                id: layoutId,
                cols: [
                    template,
                    uploadButton,
                    clearButton,
                    deleteButton,
                    formField,
                    {}
                ]
            };

            if (params.view !== 'toolbar') {
                this.afterBuild(() => {
                    $$(layoutId + '_template').bind(this.getDataWidget().config.id);
                });

                layout.cols.unshift(label);
            }

            return layout;
        } else {
            let list = {
                view: "list",
                id: layoutId + '_list',
                type: "uploader",
                autoheight: true,
                borderless: true
            }

            webix.ui({
                view: 'uploader',
                id: layoutId + '_uploader',
                multiple: false,
                autosend: false,
                upload: '/admin/upload/image',
                link: layoutId + '_list',
                apiOnly: true,
                on: {
                    onAfterFileAdd: (upload) => {
                        $$(layoutId + '_clear_button').show();
                    }
                }
            });

            formField.id = layoutId + '_value';
            formField.hidden = true;
            formField.related = layoutId + '_uploader';
            formField.on = {
                onChange: (value) => {
                    this.clearUploader(layoutId);
                }
            }

            let layout = {
                id: layoutId,
                cols: [
                    uploadButton,
                    clearButton,
                    deleteButton,
                    list,
                    formField
                ]
            };

            if (params.view !== 'toolbar') {
                layout.cols.unshift(label);
                layout.cols.push({});
            }

            return layout;
        }
    }
}