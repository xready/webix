class XR_Webix_Sidebar extends XR_Webix_Widget {
    viewSuffix = '_sidebar';

    constructor(params, widgets) {
        super(params);

        if (params.collapsed === null) {
            params.collapsed = webix.env.mobile;
        }

        this.layoutConfig = {
            cols: [
                {
                    id: this.id + this.viewSuffix,
                    view: "sidebar",
                    collapsed: params.collapsed,
                    position: "left",
                    on: {
                        onAfterSelect: (id) => {
                            XR.Webix.setHash('view', id);

                            this.$$('multiview').setValue(id);

                            this.affectIDs(this.$$().getItem(id));

                            //После выбора элемента скрываем popup, а то он остается, не помню уже где
                            this.$$().getPopup().hide();
                        },
                    },
                    data: params.data
                },
                {
                    id: this.id + "_multiview",
                    cells: widgets
                }
            ]
        }

        this.afterBuild(() => {
            this.$$().attachEvent('onMouseMove', (id) => { // Важно здесь, чтобы отработать после "системного" _showPopup.
                // На iPhone по тыку в пункт меню отрабатывает _showPopup, но не отрабатывает select.
                // Если событие мыши не mousemove (т.е. телефон, возможно только iPhone) и нет детей (ветки у дерева), то отрабатываем select и скрываем popup.
                if (!this.$$().isBranch(id) && webix.env.mouse.move !== 'mousemove') {
                    this.$$().getPopup().hide();
                    this.$$().select(id);
                }
            })

            for (let i in widgets) {
                let element = this.$$().getItem(widgets[i].id);

                if (element) {
                    element.affectIDs = widgets[i].id;
                }
            }
        });

        let hashView = XR.Webix.getHash('view');

        if (hashView !== null && $$$(hashView)) {
            params.startId = hashView;
        }

        if (params.startId !== null) {
            //Если есть первичный элемент, выбираем его после того как этот элемент построен
            $$$(params.startId).afterBuild(() => {
                this.$$().select(params.startId);
            });
        }
    }
}