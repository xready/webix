XR.Webix = {
    baseUrl: '',
    dataUrl: '',
    tinyMCEApiKey: '',
    components: {},
    mixins: {},
    App: {},
    hash: null,
    xtemplate(data, template, value, column, common) {
        if (XR.notEmpty(value)) {
            let ret = '';

            if (typeof template === "string") {
                ret += template;

                for (let field in data) {
                    ret = ret.replaceAll('#'+field+'#', data[field]);
                }
            } else if (typeof template === "boolean") {
                if (XR.exists('$group', data) && data.$group) {
                    if (XR.exists('treetable', common)) {
                        ret += common.treetable(data, common)
                    }
                }

                if (XR.exists('collection', column)) {
                    let item = column.collection.getItem(value);
                    if (item) {
                        ret += item.value;
                    }
                } else {
                    ret += value;
                }
            }

            return ret;
        }

        return '';
    },
    add(id, object) {
        this.components[id] = object
    },
    get(id) {
        return this.components[id];
    },
    widgetsBuildCallbacks: [],
    addWidgetBuildCallback(callback) {
        if (XR.isFunction(callback)) {
            this.widgetsBuildCallbacks.push(callback);
        }
    },
    applyWidgetsBuildCallbacks() {
        for (let i in this.widgetsBuildCallbacks) {
            this.widgetsBuildCallbacks[i]();
        }

        this.widgetsBuildCallbacks = [];
    },

    collections: {},

    /*
     * @param {string} parent Parent model
     * @param {object} relation
     * @return {webix.DataCollection|null}
     */
    getCollection(parent, relation) {
        let collectionUrl = null;
        let key = relation.table;

        if (relation.name !== null) {
            collectionUrl = 'xr->' + parent + '/combo/' + relation.name;
        } else if (relation.table !== null && relation.id !== null) {
            collectionUrl = 'xr->' + parent + '/combo_table/' + relation.table + '/id/' + relation.id;
        }

        if (collectionUrl !== null) {
            if (relation.attribute !== null) {
                collectionUrl += '/attribute/' + relation.attribute;
            }

            if (relation.values !== null) {
                collectionUrl += '/values/' + relation.values;
            }

            if (!XR.exists(key, this.collections)) {
                this.collections[key] = {};
            }

            if (!XR.exists(collectionUrl, this.collections[key])) {
                this.collections[key][collectionUrl] = {
                    url: collectionUrl,
                    loaded: false,
                    webix: new webix.DataCollection({})
                };
            }

            return this.collections[key][collectionUrl];
        }

        return null;
    },
    loadCollection(collection, force, callback) {
        if (!collection.loaded || force) {
            collection.webix.url = collection.url;
            collection.webix.clearAll();
            collection.webix.load(collection.webix.url).then(() => {
                collection.loaded = true;
                XR.callCallback(callback);
            });
        } else {
            XR.callCallback(callback);
        }
    },
    reloadCollections(key) {
        if (XR.exists(key, this.collections)) {
            for (let url in this.collections[key]) {
                this.loadCollection(this.collections[key][url], true);
            }
        }
    },
    parseHash() {
        if (this.hash === null) {
            this.hash = {};

            if (location.hash !== '') {
                let split1 = location.hash.substr(1).split('&');

                if (split1.length > 0) {


                    for (let i in split1) {
                        let split2 = split1[i].split('=');

                        if (split2.length === 2) {
                            this.hash[split2[0]] = split2[1];
                        }
                    }
                }
            }
        }
    },
    buildHash() {
        if (this.hash !== null) {
            let strings = [];

            for (let field in this.hash) {
                strings.push(field + '=' + this.hash[field]);
            }

            location.hash = strings.join('&');
        }
    },
    getHash(field) {
        this.parseHash();

        if (XR.exists(field, this.hash)) {
            return this.hash[field];
        }

        return null;
    },
    setHash(field, value) {
        this.parseHash();
        this.hash[field] = value;
        this.buildHash();
    },
    removeHash(field) {
        this.parseHash();
        delete this.hash[field];
        this.buildHash();
    }
}

function $$$(id) {
    return XR.Webix.get(id);
}

Object.assign($$$, XR);