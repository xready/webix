#!/bin/sh
echo "Check Webix directory"
if [ ! -d "webix" ]
then
  mkdir webix
  fi

cd webix

echo "Load or reload Webix GPL edition"
if [ -d "gpl" ]
then
  echo "Remove current directory"
  rm -rf gpl
  fi

echo "Download Webix"
wget --quiet --no-check-certificate https://webix.com/packages/webix.zip

if [ ! -f "webix.zip" ]
then
  echo "Download error";
  exit 0;
  fi

echo "Unpack Webix"
unzip -q webix.zip -d webix.gpl
echo "Move to correct directory"
mv webix.gpl/codebase gpl
echo "Remove redundant"
rm -rf webix.gpl
rm -rf webix.zip