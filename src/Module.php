<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace xr\webix;

use xr\webix\bundles\ModuleAsset;
use yii\base\BootstrapInterface;
use yii\di\Instance;
use yii\helpers\Url;

class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'xr\webix\controllers';

    public null|array|Admin $admin = null;
    public array $extraAssets = [];
    public string $tinyMCEApiKey = '';


    /**
     * {@inheritdoc}
     */
    public function bootstrap($app)
    {
        if ($app instanceof \yii\web\Application) {
            $app->getUrlManager()->addRules([
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id, 'route' => $this->id . '/admin/index'],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [$this->id.'/data' => $this->id.'/data'],
                    'tokens' => [
                        '{id}' => '<id>'
                    ],
                    'extraPatterns' => [
                        'POST create' => 'create',
                        'POST validate' => 'validate',
                        'one/<id>' => 'one'
                    ]
                ],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id.'/upload', 'route' => $this->id.'/upload'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id.'/form/handle', 'route' => $this->id.'/form/handle'],
            ], false);

            ModuleAsset::$dataUrl = Url::to(['/'.$this->id.'/data/']);
            ModuleAsset::$mainUrl = Url::to(['/'.$this->id]);
            ModuleAsset::$tinyMCEApiKey = $this->tinyMCEApiKey;
            ModuleAsset::$extraAssets = $this->extraAssets;
        } elseif ($app instanceof \yii\console\Application) {

        }

        $this->setAliases([
            '@webix' => $this->basePath
        ]);
    }

    public function init() {
        parent::init();

        if ($this->admin !== null) {
            $this->admin = Instance::ensure($this->admin, Admin::class);
        }
    }

    public function __construct($id, $parent = null, $config = []) {
        parent::__construct($id, $parent, $config);
    }
}
