<?php

namespace xr\webix\widgets;

use xr\webix\Webix;

class Form extends Widget {
    public ?string $formClass = null;
    public string $view = 'form';
    public ?string $dataWidget = null;
    public array $fields;
    public bool $clearAfterAdd = true;
    public string $saveButton   = 'Sumbit';
    public ?string $labelWidth = null;

    protected \xr\webix\models\Form $form;

    public function init() {
        parent::init();

        $this->formClass = Webix::checkForm($this->formClass);
        $this->form = new ($this->formClass)();

        $this->fields = $this->form->getWebixFields();
    }

    public function run() {
        $this->formClass = str_replace('\\', '-', $this->formClass);

        return parent::run();
    }
}