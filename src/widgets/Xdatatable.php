<?php

namespace xr\webix\widgets;

use xr\webix\Webix;
use xr\webix\models\ActiveRecord;
use yii\web\View;

class XDatatable extends Datatable {
    public function getExtraJSFiles() {
        return ['data.js', 'datatable.js'];
    }
}