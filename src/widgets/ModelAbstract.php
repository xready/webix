<?php

namespace xr\webix\widgets;

use xr\webix\Webix;
use xr\webix\models\ActiveRecord;
use yii\web\View;

abstract class ModelAbstract extends Widget {
    public ?string $modelClass = null;
    public array $columns;
    public bool $create = false;
    public bool $update = false;
    public bool $delete = false;
    public string $tableName;
    public bool $newDataPrepend = true;
    public array $primaryKeys = [];

    protected ActiveRecord $model;

    public function init() {
        parent::init();

        $this->modelClass = Webix::checkActiveRecord($this->modelClass);

        $this->model = new ($this->modelClass)();

        $this->tableName = $this->model::tableName();
        $this->primaryKeys = $this->model::primaryKey();
        $this->columns = $this->model->getWebixColumns();
        $this->create = $this->model->create;
        $this->update = $this->model->update;
        $this->delete = $this->model->delete;
    }

    public function getInlineParams(View $view): string {
        $rules = $this->model->getWebixRules($view);

        $ret = '{';

        foreach ($rules as $attribute => $rule) {
            $ret .= $attribute.': (value, messages) => {';
            $ret .= $rule;
            $ret .= '},';
        }

        $ret .= '}';

        return ", $ret".parent::getInlineParams($view);
    }

    public function run() {
        $this->modelClass = str_replace('\\', '-', $this->modelClass);

        return parent::run();
    }
}