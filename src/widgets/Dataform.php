<?php

namespace xr\webix\widgets;

use xr\webix\Webix;
use xr\webix\models\ActiveRecord;

class Dataform extends ModelAbstract {
    public string $view = 'form';
    public ?string $dataWidget = null;
    public ?string $modelClass = null;
    public bool $clearAfterAdd = true;
    public string $saveButton   = 'Sumbit';

    public function getExtraJSFiles() {
        return ['form.js'];
    }
}