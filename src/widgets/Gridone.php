<?php

namespace xr\webix\widgets;

use xr\webix\Webix;
use xr\webix\models\ActiveRecord;

class Gridone extends Data {
    public bool $autoload = false;
    public bool $paging = false;
}