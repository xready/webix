<?php

namespace xr\webix\widgets;

class Template extends Widget {
    public string $template;
    public string|array $css = '';
    public ?string $src = null;
}