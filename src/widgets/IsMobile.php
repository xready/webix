<?php

namespace xr\webix\widgets;

use Detection\MobileDetect;

class IsMobile extends Widget {
    public ?array $mobile = null;
    public ?array $desktop = null;
    public bool $tabletMobile = false;

    public function run() {
        if ($this->mobile === null || $this->desktop === null) {
            throw new \Exception('You have to specify only both mobile and desktop widgets');
        }

        if (!array_key_exists('id', $this->mobile)) {
            $this->mobile['id'] = $this->id;
        }

        if (!array_key_exists('id', $this->desktop)) {
            $this->desktop['id'] = $this->id;
        }

        if ($this->title !== null) {
            if (!array_key_exists('title', $this->mobile)) {
                $this->mobile['title'] = $this->title;
            }

            if (!array_key_exists('title', $this->desktop)) {
                $this->desktop['title'] = $this->title;
            }
        }

        $check = new MobileDetect();
        if ($check->isMobile() && ($this->tabletMobile || !$check->isTablet())) {
            $widget = $this->parseWidget($this->mobile, $this->mode);
        } else {
            $widget = $this->parseWidget($this->desktop, $this->mode);
        }

        return $widget;
    }
}