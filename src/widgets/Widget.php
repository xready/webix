<?php

namespace xr\webix\widgets;

use yii\base;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\web\View;

abstract class Widget extends base\Widget {
    public string $name;
    public ?string $id = null;
    public ?string $container = null;
    public string $mode = 'div'; //div/script/js/object
    public bool $hidden = false;
    public int $gravity = 1;
    public ?string $title = null;
    public array $extra = [];

    private static int $widgetCount = 1;

    protected function prepareDataItemId(&$item) {
        if (!array_key_exists('id', $item)) {
            if (!is_string($item['widget']) && array_key_exists('id', $item['widget'])) {
                $item['id'] = $item['widget']['id'];
            } else {
                $item['id'] = uniqid();
            }
        }

        if (!is_string($item['widget'])  && !array_key_exists('id', $item['widget'])) {
            $item['widget']['id'] = $item['id'];
        }
    }

    protected function parseWidget($widget, $mode = 'js') {
        if (!is_string($widget)) {
            $widget['mode'] = $mode;
            $widget = $widget['class']::widget($widget);
        }

        return $widget;
    }

    protected function parseWidgets(&$widgets, $mode = 'js') {
        foreach ($widgets as &$widget) {
            $widget = $this->parseWidget($widget, $mode);
        }
    }

    public function init() {
        parent::init();

        $this->name = Inflector::camel2id(StringHelper::basename(get_called_class()), '_');

        if ($this->id === null) {

            $this->id = 'xr_widget_'.$this->name.'_'.self::$widgetCount;
            self::$widgetCount++;
        }

        if ($this->container === null && $this->mode === 'div') {
            $this->container = $this->id.'_container';
        }
    }

    public function getInlineParams(View $view) : string {
        return '';
    }

    public function getBeforeCall() : string {
        return '';
    }

    public function getExtraJSFiles() {
        return [];
    }

    public function call(View $view) {
        $call = '';

        $call .= $this->getBeforeCall()."\n";

        $call .= 'new XR_Webix_'.ucfirst($this->name).'('.json_encode($this, JSON_PRETTY_PRINT).$this->getInlineParams($view).')';

        switch ($this->mode) {
            case 'js':
                $call .= '.config()';
                break;
            case 'div':
            case 'script':
                $call .= '.build();';
                break;
            case 'object':
                break;
        }

        return $call;
    }

    public function run() {
        return $this->render('widget', [
            'widget' => $this
        ]);
    }
}