<?php

namespace xr\webix\widgets;

use yii\web\View;

class Layout extends Widget {
    protected array $rows = [];
    protected array $cols = [];

    public bool $fullScreen = false;

    public function setRows($rows) {
        $this->rows = $rows;
    }

    public function setCols($cols) {
        $this->cols = $cols;
    }

    public function getInlineParams(View $view): string {
        return ', ['.implode(',', $this->rows).'], ['.implode(',', $this->cols).']';
    }

    public function getBeforeCall(): string {
        if ($this->fullScreen && $this->mode === 'script') {
            return 'webix.ui.fullScreen();';
        }

        return '';
    }

    public function run() {
        $this->parseWidgets($this->rows);
        $this->parseWidgets($this->cols);

        return parent::run();
    }
}