<?php

namespace xr\webix\widgets;

use yii\web\View;

class Data extends ModelAbstract {
    public bool $autoload = false;
    public ?array $affectLinks = null;
    public string $formPosition = 'bottom';
    public string $edit = 'inline';
    public string $insertMethod = 'inline';
    public string $updateMethod = 'inline';

    public bool|string $reloadButton = false;
    public bool|string $addButton    = false;
    public string $saveButton   = 'Sumbit';

    public bool    $paging = true;
    public int $pageSize = 20;

    public array   $childrenData = [];
    public bool    $childrenAfterAdd = true;
    public bool    $childrenResetAfterOpen = false;
    public string  $childrenTitle = 'Запись'; //Заголовок детей
    public string  $childrenTitleNew = 'Запись';  //Заголовок детей для новой записи
    public string  $childrenTitleForm = 'Запись'; //Заголовок таба формы в multiview
    public bool    $clearFormAfterAdd = true;

    protected ?array $children = null;

    private ?string $inlineForm = null;
    private ?string $childrenLayout = null;

    public function __construct($config = []) {
        parent::__construct($config);
    }

    /**
     * @param array|null $children
     * @return Data
     */
    public function setChildren(?array $children) {
        $this->children = $children;
    }

    private function setChildProperty(&$child, $property, $value = null) {
        if ($value === null && is_array($child)) {
            if (array_key_exists($property, $child)) {
                $childId = $child['id'];

                if (!array_key_exists($childId, $this->childrenData)) {
                    $this->childrenData[$childId] = [];
                }

                $this->childrenData[$childId][$property] = $child[$property];
            }
        } else {
            if (is_array($child)) {
                $childId = $child['id'];
            } else {
                $childId = $child;
            }

            if (!array_key_exists($childId, $this->childrenData)) {
                $this->childrenData[$childId] = [];
            }

            $this->childrenData[$childId][$property] = $value;
        }

    }

    private function prepareChildrenData() {
        foreach ($this->children as $child) {
            $this->prepareDataItemId($child);

            $this->setChildProperty($child, 'dataId');
            $this->setChildProperty($child, 'affectCallback');
            $this->setChildProperty($child, 'from');
            $this->setChildProperty($child, 'to');
            $this->setChildProperty($child, 'main', false);
        }
    }

    public function init() {
        parent::init();

        $this->pageSize = $this->model->limit;

        if ($this->children !== null) {
            $this->edit = 'child';
        }

        if (
            (
                $this->create &&
                $this->edit === 'inline' &&
                $this->insertMethod === 'inline'
            ) || (
                $this->edit === 'child' &&
                ($this->create || $this->update)
            )
        ) {
            $form = [
                'class' => Dataform::class,
                'id' => $this->id . '_form',
                'view' => $this->edit === 'child'?'form':'toolbar',
                'dataWidget' => $this->id,
                'modelClass' => $this->modelClass,
                'newDataPrepend' => $this->newDataPrepend,
                'clearAfterAdd' => $this->clearFormAfterAdd,
                'saveButton' => $this->saveButton
            ];
        }

        if ($this->children !== null) {
            $this->prepareChildrenData();

            if (isset($form)) {
                array_unshift($this->children, [
                    'value' => $this->childrenTitleForm,
                    'main' => '',
                    'widget' => $form
                ]);
                $this->setChildProperty($form, 'main', true);
            }

            $this->childrenLayout = $this->parseWidget([
                'class' => Tabbar::class,
                'id' => $this->id . '_children',
                'data' => $this->children
            ]);
        } else {
            if (isset($form)) {
                $this->inlineForm = $this->parseWidget($form);
            }
        }
    }

    public function getInlineParams(View $view): string {
        $inlineParams = '';

        if ($this->inlineForm !== null) {
            $inlineParams .=  ', '.$this->inlineForm;
        } else {
            $inlineParams .= ', null';
        }

        if ($this->childrenLayout !== null) {
            $inlineParams .=  ', '.$this->childrenLayout;
        } else {
            $inlineParams .= ', null';
        }

        return $inlineParams.parent::getInlineParams($view);
    }

    public function getExtraJSFiles() {
        return ['data.js', 'multiview.js'];
    }
}