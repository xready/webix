<?php

namespace xr\webix\widgets;

use yii\web\View;

class Sidebar extends Widget {
    protected array $widgets = [];
    protected ?array $start = null;

    public array $data;
    public string $mode = 'js';
    public bool $showItemTitle = true;
    public ?string $startId = null;

    public ?bool $collapsed = null;

    public function setStart($start) {
        $this->start = $start;
    }

    public function parseData(&$data) {
        foreach ($data as &$item) {
            if (array_key_exists('widget', $item)) {
                $this->prepareDataItemId($item);

                if ($this->showItemTitle && !is_string($item['widget']) && !array_key_exists('title', $item['widget'])) {
                    $item['widget']['title'] = $item['value'];
                }

                $this->widgets[] = $item['widget'];

                if ($this->startId === null) {
                    $this->startId = $item['id'];
                }

                unset($item['widget']);
            }

            if (array_key_exists('data', $item)) {
                $this->parseData($item['data']);
            }
        }
    }

    public function getInlineParams(View $view): string {
        return ', ['.implode(',', $this->widgets).']';
    }

    public function run() {
        $this->parseData($this->data);

        if ($this->start) {
            array_unshift($this->widgets, $this->start);
            $this->startId = null;
        } else {
            array_unshift($this->widgets, [
                'class'=> Template::class,
                'template' => ''
            ]);
        }

        $this->parseWidgets($this->widgets);

        return parent::run();
    }
}