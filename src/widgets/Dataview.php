<?php

namespace xr\webix\widgets;

use xr\webix\Webix;
use xr\webix\models\ActiveRecord;

class Dataview extends Data {
    public string $edit = 'child';
    public string $insertMethod = 'child';
    public string $updateMethod = 'child';
    public bool|string $addButton    = 'Add';

    public bool $showColumnTitle = true;
}