<?php

namespace xr\webix\widgets;

use yii\web\View;

class Multiview extends Widget {
    protected array $cells = [];

    public function setCells($cells) {
        $this->cells = $cells;
    }

    public function getInlineParams(View $view): string {
        return ', ['.implode(',', $this->cells).']';
    }

    public function run() {
        $this->parseWidgets($this->cells);

        return parent::run();
    }
}