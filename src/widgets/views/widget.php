<?php
/* @var $this \yii\web\View */
/* @var $widget \xr\webix\widgets\Widget */
/* @var $widgetCall string */

use xr\webix\bundles\GplAsset;
use xr\webix\bundles\ModuleAsset;

GplAsset::register($this);
ModuleAsset::addJsDir($this, 'js/widgets/mixins');
ModuleAsset::addJsDir($this, 'js/widgets/'.$widget->name);
foreach ($widget->getExtraJSFiles() as $file) {
    ModuleAsset::addJs($this, 'js/widgets/'.$file);
}
ModuleAsset::addJs($this, 'js/widgets/'.$widget->name.'.js');
ModuleAsset::register($this);
?>
<?php if ($widget->mode === 'div'): ?>
    <div style="width: 1000px; height: 500px;" id="<?=$widget->container?>"></div>
<?php endif; ?>

<?php if ($widget->mode === 'div' || $widget->mode === 'script'): ?>
    <script>
        webix.ready(function() {
            <?=$widget->call($this)?>
        });
    </script>
<?php else: ?>
    <?=$widget->call($this)?>
<?php endif; ?>