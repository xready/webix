<?php


namespace xr\webix\bundles;

use yii\web\AssetBundle;
use yii\web\View;

class GplAsset extends AssetBundle
{
    public $sourcePath = '@webix/assets/webix/gpl';
    public $jsOptions  = [ 'position' => View::POS_HEAD ];
    public $js         = [ 'webix.js' ];
    public $css        = [ 'webix.css' ];
}