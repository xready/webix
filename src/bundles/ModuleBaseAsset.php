<?php


namespace xr\webix\bundles;


use yii\web\AssetBundle;
use yii\web\View;

class ModuleBaseAsset extends AssetBundle
{
    public $sourcePath = '@webix/assets';
    public $jsOptions  = [ 'position' => View::POS_HEAD ];
    public $css        = [
        'css/webix.css'
    ];
    public $js         = [
        'js/xr.js',
        'js/webix.js',
        'js/proxy.js'
    ];
    public $depends = [
        GplAsset::class
    ];
}