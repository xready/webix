<?php


namespace app\assets\webix;


use yii\web\AssetBundle;
use yii\web\View;

class TrialAsset extends AssetBundle
{
    public $sourcePath = '@webix/assets/webix/trial';
    public $jsOptions  = [ 'position' => View::POS_HEAD ];
    public $js         = [ 'webix.js' ];
    public $css        = [ 'webix.css' ];
    //public $css        = [ 'skins/flat.css' ];
}