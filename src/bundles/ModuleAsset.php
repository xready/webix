<?php


namespace xr\webix\bundles;


use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class ModuleAsset extends AssetBundle
{
    public static string $dataUrl = '';
    public static string $mainUrl = '';
    public static string $tinyMCEApiKey = '';

    public static array $extraAssets = [];

    private static $bundle;

    private static function getBundle(View $view) {
        return $view->getAssetManager()->getBundle(self::class);
    }

    public static function addJs(View $view, $path) {
        $js = &self::getBundle($view)->js;

        if (!in_array($path, $js, true)) {
            $js[] = $path;
        }
    }

    public static function addJsDir($view, $path) {
        $sourcePath = \Yii::getAlias(self::getBundle($view)->sourcePath).'/';
        $path = $sourcePath.$path;

        if (file_exists($path)) {
            foreach (FileHelper::findFiles($path) as $file) {
                self::addJs($view, str_replace($sourcePath, '', $file));
            }
        }
    }

    public static function register($view) {
        $register = parent::register($view);

        $view->registerJs(
            'XR.Webix.dataUrl = "'.self::$dataUrl.'/";',
            View::POS_BEGIN,
            'register-data-url'
        );

        $view->registerJs(
            'XR.Webix.baseUrl = "'.self::$mainUrl.'/";',
            View::POS_BEGIN,
            'register-base-url'
        );

        $view->registerJs(
            'XR.Webix.tinyMCEApiKey = "'.self::$tinyMCEApiKey.'";',
            View::POS_BEGIN,
            'register-tiny-key'
        );

        return $register;
    }

    public $sourcePath = '@webix/assets';
    public $jsOptions  = [ 'position' => View::POS_HEAD ];
    public $css        = [
    ];
    public $js         = [
        'js/widget.events.js',
        'js/widget.js'
    ];
    public $depends = [
        GplAsset::class,
        ModuleBaseAsset::class
    ];

    public function __construct($config = []) {
        parent::__construct($config);

        $this->depends = array_merge($this->depends, self::$extraAssets);
    }
}