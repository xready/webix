<?php


namespace xr\webix\bundles;

use yii\web\AssetBundle;
use yii\web\View;

class ExtraAsset extends AssetBundle
{
    public $sourcePath = '@webix/assets/js/extra';
    public $jsOptions  = [ 'position' => View::POS_HEAD ];
    public $js         = [
        'tinymce.js'
    ];
    public $css        = [];
}