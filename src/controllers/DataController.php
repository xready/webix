<?php

namespace xr\webix\controllers;

use app\models\ImageForm;
use xr\webix\controllers\actions\DataCreateAction;
use xr\webix\controllers\actions\DataDeleteAction;
use xr\webix\controllers\actions\DataIndexAction;
use xr\webix\controllers\actions\DataUpdateAction;
use xr\webix\models\ActiveRecord;
use xr\webix\Webix;
use yii\rest\ActiveController;
use yii\web\UploadedFile;

class DataController extends ActiveController {
    public function actions() {
        $actions = parent::actions();

        $actions['index']['dataFilter'] = [
            'class' => 'yii\data\ActiveDataFilter',
            'searchModel' => $this->modelClass
        ];

        $actions['index']['class'] = DataIndexAction::class;
        $actions['create']['class'] = DataCreateAction::class;
        $actions['update']['class'] = DataUpdateAction::class;
        $actions['delete']['class'] = DataDeleteAction::class;

        return $actions;
    }

    public function __construct($id, $module, $config = []) {
        $this->modelClass = Webix::checkActiveRecord(str_replace('-', '\\', \Yii::$app->request->get('model')));

        parent::__construct($id, $module, $config);
    }

    public function actionValidate($id = null) {
        ActiveRecord::$webixSaveMode = true;

        if ($id === null) {
            $model = new ($this->modelClass)();
        } else {
            $model = ($this->modelClass)::findOne($id);
        }

        $model->setAttributes(\Yii::$app->request->post(), false);

        if ($model->validate()) {
            return ['success' => true];
        } else {
            return ['success' => false, 'errors' => $model->getErrors()];
        }
    }

    public function actionCombo($attribute, $relation, $values) {
        $model = new ($this->modelClass)();

        return $model->getCombo($attribute, $relation, explode(',', $values));
    }

    public function actionCombo_table($attribute, $table, $id, $values) {
        $model = new ($this->modelClass)();

        return $model->getComboByTable($attribute, $table, $id, explode(',', $values), $attribute);
    }

    public function actionOne($id) {
        return $this->modelClass::getOne($id);
    }

    public function actionTree($field) {
        $model = new ($this->modelClass)();

        return $model->getTree($field);
    }
}