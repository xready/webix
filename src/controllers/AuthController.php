<?php

namespace xr\webix\controllers;

use xr\webix\models\AuthForm;
use yii\helpers\Url;
use yii\rest\Controller;

class AuthController extends Controller {
    public function actionLogin() {
        if (\Yii::$app->user->isGuest) {
            $model = new AuthForm();
            $model->attributes = \Yii::$app->request->post();
            if ($model->login()) {
                return ['success' => true];
            } else {
                return ['success' => false, 'errors' => $model->getErrors()];
            }
        }

        return ['success' => false];
    }

    public function actionLogout()
    {
        if (\Yii::$app->request->isAjax) {
            if (\Yii::$app->user->isGuest) {
                return ['success' => false];
            }

            return ['success' => \Yii::$app->user->logout()];
        }

        if (!\Yii::$app->user->isGuest) {
            \Yii::$app->user->logout();
        }

        return $this->redirect(Url::to(['/'.$this->module->id]));
    }
}