<?php

namespace xr\webix\controllers;

use xr\webix\models\Upload;
use yii\rest\Controller;
use yii\web\UploadedFile;

class UploadController extends Controller {
    public function actionImage() {
        $model = new Upload();

        if (\Yii::$app->request->isPost) {
            $model->setUpload(UploadedFile::getInstanceByName('upload'));

            $upload = $model->upload();
            
            if ($upload) {
                //return ['success' => $upload, 'path' => $model->path, 'status' => 'server'];
                return ['success' => $upload, 'data' => $model, 'status' => 'server'];
            } else {
                if ($model->hasErrors()) {
                    return ['success' => false, 'status' => 'error', 'error' => $model->getErrors()['upload'][0]];
                }

                return ['success' => false, 'status' => 'error'];
            }
        }

        return ['success' => false, 'status' => 'error'];
    }
}