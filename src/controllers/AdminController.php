<?php

namespace xr\webix\controllers;

use yii\base\ViewNotFoundException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class AdminController extends Controller {
    public string $favicon = '';

    public function actionIndex() {
        try {
            $this->favicon = $this->renderPartial('@app/views/favicon');
        } catch (ViewNotFoundException) {

        }

        $admin = $this->module->admin;

        if ($admin === null) {
            throw new NotFoundHttpException('Admin is not initiated');
        }

        $this->layout = 'admin';
        $this->view->title = $admin->title;

        return $this->render('index', ['admin' => $admin]);
    }
}