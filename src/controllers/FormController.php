<?php

namespace xr\webix\controllers;

use xr\webix\models\Form;
use xr\webix\Webix;
use yii\rest\Controller;

class FormController extends Controller {
    public string $formClass;

    public function __construct($id, $module, $config = []) {
        $this->formClass = Webix::checkForm(str_replace('-', '\\', \Yii::$app->request->get('form')));

        parent::__construct($id, $module, $config);
    }

    public function actionHandle() {
        $model = new ($this->formClass)();
        $model->setAttributes(\Yii::$app->request->post(), false);

        return $model->handle();
    }
}