<?php
namespace xr\webix\controllers\actions;

use xr\webix\models\ActiveRecord;
use yii\data\Sort;
use yii\rest\CreateAction;
use yii\rest\IndexAction;

class DataCreateAction extends CreateAction {
    public function run() {
        ActiveRecord::$webixSaveMode = true;

        $model = parent::run();

        if (!$model->hasErrors()) {
            return $model->forWebix();
        }

        return $model;
    }
}