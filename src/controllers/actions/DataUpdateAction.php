<?php
namespace xr\webix\controllers\actions;

use xr\webix\models\ActiveRecord;
use yii\data\Sort;
use yii\rest\IndexAction;
use yii\rest\UpdateAction;

class DataUpdateAction extends UpdateAction {
    public function run($id) {
        ActiveRecord::$webixSaveMode = true;

        $model = parent::run($id);

        if (!$model->hasErrors()) {
            return $model->forWebix();
        }

        return $model;
    }
}