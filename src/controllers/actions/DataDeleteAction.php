<?php

namespace xr\webix\controllers\actions;

use xr\webix\models\ActiveRecord;
use yii\db\IntegrityException;
use yii\rest\DeleteAction;

class DataDeleteAction extends DeleteAction {
    public function run($id) {
        ActiveRecord::$webixSaveMode = true;

        try {
            parent::run($id);
        } catch (\Exception $e) {
            if ($e instanceof IntegrityException && $e->getCode() === '23503') {
                \Yii::$app->getResponse()->setStatusCode(424);
            } else {
                throw $e;
            }
        }
    }
}