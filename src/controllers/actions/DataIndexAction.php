<?php
namespace xr\webix\controllers\actions;

use xr\webix\models\ActiveRecord;
use yii\data\Sort;
use yii\rest\IndexAction;

class DataIndexAction extends IndexAction {
    protected function prepareDataProvider() {
        /** @var ActiveRecord $model */
        $model = \Yii::createObject($this->modelClass);

        $query = $model::find();

        $filter = \Yii::$app->request->get('filter');

        if (is_array($filter)) {
            foreach ($filter as $attribute => $value) {
                if ($model->hasAttribute($attribute) !== null) {
                    if ($model::getTableSchema()->columns[$attribute]->type === 'string') {
                        $query->andWhere(['LIKE', $attribute, $value]);
                    } elseif ($value === 'NULL' && $model->isForeignKey($attribute)) {
                        $query->andWhere([$attribute => null]);
                    } else {
                        $query->andWhere([$attribute => $value]);
                    }
                }

                //TODO: Кастомный атрибут, а затем Exception
            }
        }

        $totalCount = (clone $query)->count();

        $paging = \Yii::$app->request->get('paging', 'paging');

        if ($paging !== 'all') {
            $start = \Yii::$app->request->get('start', 0);

            $query->limit($model->limit);
            $query->offset($start);
        } else {
            $start = 0;
        }

        if ($sort = \Yii::$app->request->get('sort')) {
            $sortDirection = \Yii::$app->request->get('sort_direction', 'asc');

            $query->orderBy("$sort $sortDirection");
        } else {
            $query->orderBy($model->getDefaultOrder());
        }

        $data = $query->all();

        $returnData = [];

        foreach ($data as $row) {
            $returnData[] = $row->forWebix();
        }

        return [
            'data'        => $returnData,
            'total_count' => $totalCount,
            'pos'         => $start
        ];
    }
}