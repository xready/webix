<?php
/* @var $this \yii\web\View */
/* @var $admin \xr\webix\Admin */

use xr\webix\bundles\ModuleAsset;
use xr\webix\widgets\Auth;
use xr\webix\widgets\Layout;

ModuleAsset::addJs($this, 'js/admin.js');
ModuleAsset::register($this);
?>
<?php if (Yii::$app->user->isGuest): ?>
    <div id="auth" style="width: 100%; max-width: 500px; height: fit-content; margin-left: auto; margin-right: auto;"></div>
    <?=Auth::widget(['id' => 'auth', 'mode' => 'script', 'container' => 'auth'])?>
<?php else: ?>
    <?=Layout::widget([
        'fullScreen' => true,
        'mode' => 'script',
        'rows' => [
            "XR.Webix.Admin.header('{$admin->layout}', '{$admin->title}','{$admin->menuId}')",
            $admin->viewConfig
        ]
    ])?>
<?php endif; ?>
