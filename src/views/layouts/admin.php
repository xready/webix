<?php
/* @var $this \yii\web\View */
/* @var $content string */

use xr\webix\bundles\GplAsset;
use xr\webix\bundles\ExtraAsset;
use yii\helpers\Html;

GplAsset::register($this);
ExtraAsset::register($this)
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php /*<link rel="stylesheet" href="//cdn.webix.com/materialdesignicons/5.8.95/css/materialdesignicons.min.css" type="text/css" charset="utf-8"> */ ?>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/@mdi/font@6.1.95/css/materialdesignicons.min.css" type="text/css" charset="utf-8">
    <?php $this->head() ?>
    <?=property_exists($this->context, 'favicon')?$this->context->favicon:''; ?>
</head>
<body>
<?php $this->beginBody() ?>
<?=$content?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
