<?php

namespace xr\webix;

use xr\webix\models\Form;
use xr\webix\models\WebixRecord;
use yii\base\Exception;

class Webix {
    public static function checkActiveRecord(string|null $modelClass) : string {
        if ($modelClass === null || $modelClass === '') {
            throw new Exception('No model was provided');
        } else {
            if (class_exists($modelClass)) {
                $parentClass = WebixRecord::class;

                if (is_subclass_of($modelClass, $parentClass)) {
                    return $modelClass;
                } else {
                    throw new Exception('Model is not child of '.$parentClass);
                }
            } else {
                throw new Exception('No such class for this model');
            }

        }
    }

    public static function checkForm(string|null $modelClass) : string {
        if ($modelClass === null || $modelClass === '') {
            throw new Exception('No form was provided');
        } else {
            if (class_exists($modelClass)) {
                $parentClass = Form::class;

                if (is_subclass_of($modelClass, $parentClass)) {
                    return $modelClass;
                } else {
                    throw new Exception('Form is not child of '.$parentClass);
                }
            } else {
                throw new Exception('No such class for this form');
            }
        }
    }
}