<?php

namespace xr\webix;

use xr\webix\widgets\Sidebar;
use xr\webix\widgets\Tabbar;
use yii\base\BaseObject;

class Admin extends BaseObject {
    public array $viewConfig;
    public string $title = 'Admin';
    public string $menuId;
    public string $layout = 'sidebar';

    public function init() {
        parent::init();

        $this->viewConfig['class'] = match ($this->layout) {
            'tabbar' => Tabbar::class,
            default => Sidebar::class,
        };

        if (!array_key_exists('id', $this->viewConfig)) {
            $this->viewConfig['id'] = 'xr_webix_admin_menu';
        }

        $this->menuId = $this->viewConfig['id'];
    }
}