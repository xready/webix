<?php

namespace xr\webix\models;

use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class Upload extends Model {
    private static string $basePath = '@runtime/uploads';

    public static function fileExists($name): bool {
        return file_exists(self::getPath($name));
    }

    public static function getPath($name): bool|string {
        return \Yii::getAlias(self::$basePath.'/'.$name);
    }

    public static function move($name, $target) {
        $path = \Yii::getAlias(self::$basePath.'/'.$name);

        if (file_exists($path)) {
            rename($path, $target);
        }

        return false;
    }

    public static function getDataFromVar($var): ?self {
        $data = json_decode($var);

        if ($data) {
            $model = new self();

            $model->path      = $data->path;
            $model->name      = $data->name;
            $model->baseName  = $data->baseName;
            $model->extension = $data->extension;

            return $model;
        }

        return null;
    }

    public string $path;
    public string $name;
    public string $baseName;
    public string $extension;
    public string $marker;

    private UploadedFile $upload;

    private function checkFolder() {
        if (!file_exists(\Yii::getAlias(self::$basePath))) {
            FileHelper::createDirectory(\Yii::getAlias(self::$basePath));
        }
    }

    public function rules()
    {
        return [
            [['upload'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, bmp, gif, webp, xls, xlsx, csv, pdf', 'checkExtensionByMimeType' => false],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->path = uniqid() . '.' . $this->upload->extension;
            $this->name = $this->upload->name;
            $this->baseName = $this->upload->baseName;
            $this->extension = $this->upload->extension;
            $this->marker = 'uploaded';

            $this->checkFolder();

            $this->upload->saveAs(self::$basePath . '/' . $this->path);
            return true;
        }

        return false;
    }

    /**
     * @param UploadedFile $upload
     */
    public function setUpload(UploadedFile $upload): void {
        $this->upload = $upload;
    }

    /**
     * @return UploadedFile
     */
    public function getUpload(): UploadedFile {
        return $this->upload;
    }
}