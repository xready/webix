<?php

namespace xr\webix\models;

interface WebixRecord {
    public function getWebixColumns();
}