<?php

namespace xr\webix\models;

use yii\base\Model;

abstract class Form extends Model {
    abstract public function handle();

    public $fields;

    protected function addField($name, $label, $view) {
        $field = \Yii::createObject([
            'class' => column\Form::class,
            'name' => $name,
            'label' => $label,
            'view' => $view
        ]);

        $this->fields[] = $field;

        return $field;
    }

    public function getWebixFields() {
        return $this->fields;
    }
}