<?php

namespace xr\webix\models\trait;

use xr\webix\models\column\Column;
use yii\helpers\ArrayHelper;
use yii\web\View;

trait ColumnsTrait {
    /**
     * @var Column[]
     */
    private array $columns = [];

    public function isColumn(string $attribute) :  bool {
        return array_key_exists($attribute, $this->columns);
    }

    public function getColumn(string $attribute) : ?Column {
        if ($this->isColumn($attribute)) {
            return $this->columns[$attribute];
        }

        return null;
    }

    public function columnsOrder(): array {
        return array_keys($this->attributeLabels());
    }

    public function columnsConfig() {

    }

    public function columnsFinal() {

    }

    /**
     * @return Column[]
     */
    public function extraColumns() : array {
        return [];
    }

    public function addRelationAsColumn($attribute, $excludeSource = false) : Column {
        $column = clone $this->columns[$attribute];

        $column->table->id = $column->relation->name;

        $this->columns[$column->relation->name] = $column;

        if ($excludeSource) {
            $this->columns[$attribute]->exclude = true;
        }

        $this->extraFields[] = $column->relation->name;

        return $column;
    }

    public function addColumn($attribute, $title, $type = 'string') : ?Column {
        if (!array_key_exists($attribute, $this->columns)) {
            $this->columns[$attribute] = Column::create($attribute, $title, $type);

            $this->extraFields[] = $attribute;

            return $this->columns[$attribute];
        }

        return null;
    }

    public function getWebixColumns() : array {
        $tableSchema = self::getTableSchema();

        $this->handleForeignKeys();

        foreach ($this->attributes() as $attribute) {
            $this->columns[$attribute] = Column::createColumnFromSchema($attribute, $this, $tableSchema);
        }

        $this->columnsConfig();

        $this->columns = array_merge($this->columns, $this->extraColumns());

        foreach ($this->columns as $index => &$column) {
            $column->finalFromSchema($this);

            if ($column->exclude) {
                unset($this->columns[$index]);
            }
        }

        $this->columnsFinal();

        foreach ($this->columnsOrder() as $index => $attribute) {
            if ($this->isColumn($attribute)) {
                $this->columns[$attribute]->order = ($index * 10) + 10;
            }
        }

        ArrayHelper::multisort($this->columns, 'order');

        return array_values($this->columns);
    }

    public function getWebixRules(View $view) {
        $rules = [];

        foreach ($this->columns as $column) {
            $js = '';

            $validators = $this->getActiveValidators($column->attribute);

            foreach ($validators as $validator) {
                $validatorJs = $validator->clientValidateAttribute($this, $column->attribute, $view);

                if ($validatorJs !== null) {
                    $js .= $validatorJs ."\n";
                }
            }

            if ($js !== '') {
                $rules[$column->attribute] = $js;
            }
        }

        return $rules;
    }
}