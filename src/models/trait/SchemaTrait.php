<?php

namespace xr\webix\models\trait;

use xr\webix\models\schema\ForeignKey;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\db\TableSchema;
use yii\helpers\Inflector;

trait SchemaTrait {
    private ?array $foreignKeys = null;

    public function handleRelationId(ActiveQueryInterface|ActiveQuery $relation) {
        $primaryKeys  = ($relation->modelClass)::primaryKey();

        if (count($primaryKeys) > 1) {
            throw new Exception('Only one PK supported for Combo');
        }

        return $primaryKeys[0];
    }

    public function handleRelationValues(ActiveQueryInterface|ActiveQuery $relation) {
        $model = new ($relation->modelClass)();

        if ($model->hasAttribute('title')) {
            return ['title'];
        } elseif ($model->hasAttribute('name')) {
            return ['name'];
        } else {
            return [$this->handleRelationId($relation)];
        }
    }

    public function getRelationByColumnName($columnName) : ?array {
        $foreignRelation = str_replace('_id', '', $columnName);
        $foreignRelation = str_replace('_code', '', $foreignRelation);

        $relation = $this->getRelation($foreignRelation, false);

        if ($relation === null) {
            $foreignRelation = lcfirst(Inflector::id2camel($foreignRelation, '_'));
            $relation = $this->getRelation($foreignRelation, false);
        }

        if ($relation !== null) {
            return [
                'relation' => $relation,
                'relationName' => $foreignRelation
            ];
        }

        return null;
    }

    public function isForeignKey($attribute) : bool {
        $this->handleForeignKeys();

        return array_key_exists($attribute, $this->foreignKeys);
    }

    public function getForeignKey($attribute) : ?ForeignKey {
        if ($this->isForeignKey($attribute)) {
            return $this->foreignKeys[$attribute];
        }

        return null;
    }

    private function handleForeignKeys() {
        if ($this->foreignKeys === null) {
            $this->foreignKeys = [];

            $tableSchema = self::getTableSchema();

            foreach ($tableSchema->foreignKeys as $schemaKey) {
                $foreignKey = ForeignKey::create($schemaKey, $this);

                $this->foreignKeys[$foreignKey->field] = $foreignKey;
            }
        }
    }
}