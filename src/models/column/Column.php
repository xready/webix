<?php

namespace xr\webix\models\column;


use xr\webix\models\ActiveRecord;
use xr\webix\models\WebixRecord;
use yii\base\BaseObject;
use yii\db\TableSchema;

class Column extends BaseObject {
    public static function create($attribute, $title, $type = 'string') : self {
        return \Yii::createObject([
            'class' => self::class,
            'attribute' => $attribute,
            'table' => \Yii::createObject([
                'class' => Table::class,
                'id' => $attribute,
                'header' => $title,
                'sort' => 'server'
            ]),
            'form' => \Yii::createObject([
                'class' => Form::class,
                'name' => $attribute,
                'placeholder' => $title
            ]),
            'type' => $type
        ]);
    }

    public static function createColumnFromSchema($attribute, ActiveRecord $model, TableSchema $tableSchema) : self {
        if (array_key_exists($attribute, $tableSchema->columns)) {
            $column = self::create(
                $attribute,
                $model->getAttributeLabel($attribute),
                $tableSchema->columns[$attribute]->type,
            );

            $column->editable = !$tableSchema->columns[$attribute]->autoIncrement;
            if ($tableSchema->columns[$attribute]->isPrimaryKey !== null) {
                $column->primaryKey = $tableSchema->columns[$attribute]->isPrimaryKey;
            }

        } else {
            $column = self::create(
                $attribute,
                $model->getAttributeLabel($attribute)
            );
        }

        return $column;
    }

    public bool    $editable   = false;
    public bool    $sortable   = true;
    public bool    $filterable = false;
    public bool    $total      = false;
    public bool    $primaryKey = false;

    public string  $attribute;
    public Table   $table;
    public Form    $form;
    public string  $type;
    public int     $order = 9999;
    public bool    $exclude = false;
    public ?array  $options = null;
    public ?Relation $relation = null;
    public ?string $relation_values = null;
    public string|int|bool|null $defaultFilter = null;

    public function finalFromModel(WebixRecord $model) {
        if ($this->sortable) {
            $this->table->sort = 'server';
        }

        if ($this->filterable) {
            $filter = 'serverFilter';

            switch ($this->type) {
                case 'combo':
                    $filter = 'serverSelectFilter';
                    break;
            }

            $this->table->header = [ $this->table->header , [ 'content' => $filter ] ];
        }

        if ($this->editable && ($model->create || $model->update)) {
            switch ($this->type) {
                case 'date':
                    $this->form->view = 'datepicker';

                    if ($model->update) {
                        $this->table->editor = 'date';
                    }
                    break;
                case 'boolean':
                case 'tinyint':
                    $this->form->view = 'checkbox';
                    $this->form->labelWidth = 0;
                    $this->form->labelRight = $this->form->placeholder;

                    if ($model->update) {
                        $this->table->editor = 'checkbox';
                        $this->table->template = '{common.checkbox()}';
                        $this->table->options = [
                            "true" => "On",
                            "false" => "Off",
                            "undefined" => "Off",
                            "null" => "Off"
                        ];
                    }

                    break;
                case 'image':
                case 'file':
                    $this->form->view = $this->type;

                    if ($model->update) {
                        $this->table->editor = 'text';
                    }
                    break;
                case 'text':
                    $this->form->view = 'textarea';

                    if ($model->update) {
                        $this->table->editor = null;
                    }
                    break;
                case 'rich':
                case 'richtext':
                case 'wysiwyg':
                    $this->form->view = 'xr-tinymce';

                    if ($model->update) {
                        $this->table->editor = null;
                    }
                    break;
                case 'combo':
                    $this->form->view = 'combo';

                    if ($model->update) {
                        $this->table->editor = 'combo';
                    }
                    break;
                case 'hidden':
                    $this->form->view = 'hidden';
                    break;
                default:
                    $this->form->view = 'text';

                    if ($model->update) {
                        $this->table->editor = 'text';
                    }
            }
        }

        if ($this->options !== null) {
            $this->form->options = $this->options;
            $this->table->options = $this->options;
        }
    }

    public function finalFromSchema(ActiveRecord $model) {
        if ($model->isForeignKey($this->attribute)) {
            Relation::create($this, $model->getForeignKey($this->attribute), $model);
        }

        if ($this->relation !== null) {
            $this->type = 'combo';
        }

        $this->finalFromModel($model);
    }

    public function searchRelation(ActiveRecord $model) {
        Relation::search($this, $model);
    }
}