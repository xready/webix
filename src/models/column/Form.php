<?php

namespace xr\webix\models\column;

use yii\base\BaseObject;

class Form extends BaseObject {
    public string $name;
    public string $placeholder;
    public string $view;
    public string $label;
    public string $labelWidth;
    public string $labelRight;
    public ?string $value;
    public bool|string $clear = false;
    public bool   $hidden = false;
    public bool   $clearAfterAdd = true;
    public bool   $focusAfterAdd = false;
    public string|int|bool|null $defaultValue = null;
    public ?string $tinyMCEToolbar = null;
    public array $options;
}