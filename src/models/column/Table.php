<?php

namespace xr\webix\models\column;

use yii\base\BaseObject;

class Table extends BaseObject {
    public string       $id;
    public string|array $header;
    public ?string      $footer = null;
    public ?string      $sort = null;
    public ?string      $editor = null;
    public ?string          $template = null;
    public null|string|bool $xtemplate = null;
    public bool         $fillspace = true;
    public bool|string  $adjust = false;
    public bool         $hidden = false;
    public bool         $mobileHidden = false;
    public ?int         $width = null;
    public array        $options;
    public array        $extra;
}