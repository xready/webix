<?php

namespace xr\webix\models\column;

use xr\webix\models\ActiveRecord;
use xr\webix\models\schema\ForeignKey;
use yii\base\BaseObject;

class Relation extends BaseObject {
    /**
     * @var string|null Name of foreign model
     */
    public ?string $name = null;

    /**
     * @var string|null Name of foreign table
     */
    public ?string $table = null;

    /**
     * @var string|null Attribute's name of current model
     */
    public ?string $attribute = null;

    /**
     * @var string|null The attribute of an external table or model by which the model is linked
     */
    public ?string $id = null;

    /**
     * @var string|null Attributes of foreign table or model which values will be show
     */
    public ?string $values = null;

    public static function create(Column $column, ForeignKey $foreignKey, ActiveRecord $model) {
        if ($foreignKey->relation !== null) {
            $column->relation = \Yii::createObject([
                'class' => self::class,
                'attribute' => $column->attribute,
                'table' => $foreignKey->table,
                'name' =>  $foreignKey->relationName,
                'values' =>  $column->relation_values??implode(',', $model->handleRelationValues($foreignKey->relation))
            ]);
        } elseif ($column->relation_values !== null) {
            $column->relation = \Yii::createObject([
                'class' => self::class,
                'attribute' => $column->attribute,
                'table' => $foreignKey->table,
                'id' => $foreignKey->foreignField,
                'values' => $column->relation_values
            ]);
        }
    }

    public static function search(Column $column, ActiveRecord $model) {
        $relationSearch = $model->getRelationByColumnName($column->attribute);

        if ($relationSearch !== null) {
            $column->relation = \Yii::createObject([
                'class' => self::class,
                'attribute' => $column->attribute,
                'table' => $relationSearch['relation']->modelClass::tableName(),
                'name' =>  $relationSearch['relationName'],
                'values' =>  $column->relation_values??implode(',', $model->handleRelationValues($relationSearch['relation']))
            ]);
        }
    }
}