<?php
namespace xr\webix\models\schema;

use xr\webix\models\ActiveRecord;
use yii\base\BaseObject;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\helpers\Inflector;

class ForeignKey extends BaseObject {
    public string $table;
    public string $field;
    public string $foreignField;
    public ActiveQueryInterface|ActiveQuery|null $relation = null;
    public string $relationName;

    public static function create(array $schemaKey, ActiveRecord $model) : self {
        foreach ($schemaKey as $schemaKeyIndex => $schemaKeyValue) {
            if (is_numeric($schemaKeyIndex)) {
                $foreignKey = [
                    'class' => self::class,
                    'table' =>  $schemaKeyValue
                ];
            } else {
                $foreignKey['field'] = $schemaKeyIndex;
                $foreignKey['foreignField'] = $schemaKeyValue;
            }
        }

        $relation = $model->getRelationByColumnName($foreignKey['field']);

        if ($relation !== null) {
            $foreignKey = array_merge($foreignKey, $relation);
        }

        return \Yii::createObject($foreignKey);
    }
}