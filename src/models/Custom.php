<?php

namespace xr\webix\models;

use yii\base\ArrayableTrait;
use yii\base\Model;

abstract class Custom extends Model implements WebixRecord {
    use ArrayableTrait;

    public static function find() {
        return new static();
    }

    abstract public function data() : array;

    public function forWebix() {
        $data = $this->toArray();

        foreach ($data as $attribute => &$value) {
            if (($value === null || $value === '') && $this->isForeignKey($attribute)) {
                $value = 'NULL';
            }
        }

        return $data;
    }

    public function all() {
        return $this->data();
    }

    public function count() {
        return count($this->data());
    }

    public function limit() {}

    public function offset() {}

    public function orderBy() {}

    public function getDefaultOrder() {

    }

    public function getWebixColumns() {

    }
}