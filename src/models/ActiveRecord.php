<?php

namespace xr\webix\models;

use xr\db\BaseActiveRecord;
use xr\webix\models\trait\ColumnsTrait;
use xr\webix\models\trait\SchemaTrait;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\db\Command;
use yii\db\Query;

class ActiveRecord extends BaseActiveRecord implements WebixRecord {
    use ColumnsTrait;
    use SchemaTrait;

    public static $webixSaveMode = false;

    public static function getOne($id) {
        $result = [];

        if ($id !== null && $id !== 'NULL') {
            $record = self::findOne($id);

            if ($record !== null) {
                $record->getWebixColumns();
                $order = [];

                foreach ($record->toArray() as $field => $value) {
                    $column = $record->getColumn($field);

                    if ($column !== null && !$column->exclude) {
                        if (is_array($value) && $column->relation !== null) {
                            $values = [];

                            if ($column->relation_values === null) {
                                $valueFields = $record->handleRelationValues($record->getRelation($column->relation->name));
                            } else {
                                $valueFields = explode(',', $column->relation_values);
                            }

                            foreach ($valueFields as $valueId) {
                                $values[] = $value[$valueId];
                            }

                            $result[] = [
                                'id' => $field,
                                'value' => implode(' ', $values),
                                'label' => $column->table->header
                            ];
                        } else {
                            $result[] = [
                                'id' => $field,
                                'value' => $value,
                                'label' => $column->table->header
                            ];
                        }

                        $order[] = $column->order;
                    }
                }

                array_multisort($order, SORT_ASC, $result);
            }
        }

        return $result;
    }

    public bool $create   = true;
    public bool $update   = true;
    public bool $delete   = true;
    public int $limit = 20;

    protected array $extraFields = [];

    public $value; //For Webix Tree

    public function getDefaultOrder() : array|string {
        $order = [];

        foreach (self::primaryKey() as $key) {
            $order[$key] = SORT_DESC;
        }

        return $order;
    }

    public function __construct($config = []) {
        parent::__construct($config);

        if (static::$tableType === self::TYPE_VIEW) {
            $this->create = false;
            $this->update = false;
            $this->delete = false;
        }
    }

    public function __set($name, $value): void {
        if (self::$webixSaveMode) {
            switch ($value) {
                case 'NULL':
                case '':
                    if ($this->isForeignKey($name)) {
                        $value = null;
                    }
                    break;
                case 'false':
                    $value = false;
                    break;
                case 'true':
                    $value = true;
                    break;
            }
        }

        parent::__set($name, $value);
    }

    public function __get($name) {
        if ($name === 'id') {
             return $this->getId();
        }

        return parent::__get($name); // TODO: Change the autogenerated stub
    }

    public function forWebix() : array {
        $data = $this->toArray();

        foreach ($data as $attribute => &$value) {
            if (($value === null || $value === '') && $this->isForeignKey($attribute)) {
                $value = 'NULL';
            }
        }

        return $data;
    }

    public function fields() {
        return array_merge(['id'], parent::fields(), $this->extraFields); // TODO: Change the autogenerated stub
    }

    public function getId() {
        $return = [];

        foreach (static::primaryKey() as $key) {
                //$return[] = $this->_attributes[$key];
                $return[] = $this->getAttribute($key);
        }

        return implode(',', $return);
    }

    protected function parseCombo($attribute, $data, $idField, $fields) : array {
        $return = [];

        if (self::getTableSchema()->columns[$attribute]->allowNull) {
            $return[] = [
                'id' => 'NULL',
                'value' => '-'
            ];
        }

        foreach ($data as $item) {
            $values = [];

            foreach ($fields as $field) {
                $values[] = $item->$field;
            }

            $return[] = [
                'id' => $item->$idField,
                'value' => implode(' ', $values)
            ];
        }

        return $return;
    }

    protected function prepareCombo(ActiveQueryInterface|ActiveQuery $relation, $primaryKey, array $fields = []) : ActiveQuery {
        return ($relation->modelClass)::find()->select(array_merge([$primaryKey], $fields));
    }

    public function getCombo($attribute, $relationName, array $fields = []) {
        $relation = $this->getRelation($relationName);

        $primaryKey = $this->handleRelationId($relation);

        if (count($fields) === 0) {
            $fields = $this->handleRelationValues($relation);
        }

        $data = $this->prepareCombo($relation, $primaryKey, $fields)->all();

        return $this->parseCombo($attribute, $data, $primaryKey, $fields);
    }

    protected function prepareComboByTable($table, $idField, array $fields) : Command {
        $query = (new Query())->from($table)->select(array_merge([$idField], $fields));
        $command = $query->createCommand();
        $command->fetchMode = \PDO::FETCH_OBJ;

        return $command;
    }

    public function getComboByTable($attribute, $table, $idField, array $fields) : array {
        $data = $this->prepareComboByTable($table, $idField, $fields)->queryAll();

        return $this->parseCombo($attribute, $data, $idField, $fields);
    }

    public function getTree($field) {
        $data = self::find()->all();

        $tree = [];

        $treeById = [];

        foreach ($data as &$item) {
            $item->value = $item->$field;

            $treeById[$item->id] = $item;
        }

        foreach ($data as &$item) {
            //foreach ()
        }

        foreach ($treeById as $id => &$item) {
            //if ($item->pa)
        }

        return $data;
    }
}